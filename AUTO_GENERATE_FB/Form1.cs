﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using DAL;
using BLL;
using System.Xml.Serialization;

namespace AUTO_GENERATE_FB
{
	public partial class frmMain : Form
	{
		public frmMain()
		{
			InitializeComponent();
		}

		BackgroundWorker oBG;
		BackgroundWorker oBG_ProjectFile;

		List<clsObjectDescription> _lstObjectDescription;
		List<clsSoftwareObjectActuator> _lstSoftWareObjectActuators;
		List<clsSoftwareObjectDetection> _lstSoftwareObjectDetections;

		private int _iLastRowIndex = 0;
		private int _iLastColumnIndex = 0;

		private void cmbObjectType_Load(object sender, EventArgs e)
		{

			// BG for reading excel files
			oBG = new BackgroundWorker();
			oBG.RunWorkerCompleted += OBG_RunWorkerCompleted;
			oBG.DoWork += OBG_DoWork;
			oBG.RunWorkerAsync();

			// Backgroundworker to read configuration at startup of application
			oBG_ProjectFile = new BackgroundWorker();
			oBG_ProjectFile.RunWorkerCompleted += OBG_ProjectFile_RunWorkerCompleted;
			oBG_ProjectFile.DoWork += OBG_ProjectFile_DoWork;
			btnAddObject.Enabled = false;

			chbAlarmWarningsHMI.Checked = true;
			chbAlarmWarnings.Checked = true;
			chbDetections.Checked = true;
			chbObject.Checked = true;

			btnAddObject.Enabled = false;

			_lstObjectDescription = new List<clsObjectDescription>();
			_lstSoftWareObjectActuators = new List<clsSoftwareObjectActuator>();
			_lstSoftwareObjectDetections = new List<clsSoftwareObjectDetection>();

		}

		private void OBG_ProjectFile_DoWork(object sender, DoWorkEventArgs e)
		{
			List<string> lstArgs = (List<string>)e.Argument;

			clsComp.InitCodeGeneration();

			bool xAlarmWarning = clsEnt.ConvertToBool(lstArgs[1]);
			bool xObjects = clsEnt.ConvertToBool(lstArgs[2]);
			bool xDetections = clsEnt.ConvertToBool(lstArgs[3]);
			bool xAlarmWarningHMI = clsEnt.ConvertToBool(lstArgs[4]);

			clsComp.CreateSources(xAlarmWarning,xObjects,xDetections,lstArgs[0]);
			clsComp.CreateHMIObjects(xAlarmWarningHMI, Application.StartupPath, lstArgs[0]);

		}

		private void OBG_ProjectFile_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			MessageBox.Show("Files gegenereerd.", "Code generator", MessageBoxButtons.OK, MessageBoxIcon.Information);

		}





		#region Startup Backgroundworker

		// Get available PLC object types
		private void OBG_DoWork(object sender, DoWorkEventArgs e)
		{
			clsComp.GetPLCObjectTypes(Application.StartupPath);
		}

		private void OBG_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			
			foreach (clsGenericObjectType item in clsComp.GenericObjects)
			{
				cmbGenericObjects.Items.Add(item.ObjectName);
				cmbGenericObjectSensors.Items.Add(item.ObjectName);
			}

		}

		#endregion






		// Start genereren code
		private void btnAddObject_Click(object sender, EventArgs e)
		{
			// Displays an OpenFileDialog so the user can select a Cursor.
			SaveFileDialog oSfdSave = new SaveFileDialog();
			oSfdSave.Filter = "Source Files|*.scl";
			oSfdSave.Title = "Select target location for sources";
			oSfdSave.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

			List<string> lstArgs;
			lstArgs = new List<string>();

			// Show the Dialog.
			if (oSfdSave.ShowDialog() == System.Windows.Forms.DialogResult.OK)
			{
				lstArgs.Add(oSfdSave.FileName);// 0
				lstArgs.Add(chbAlarmWarnings.Checked.ToString());// 1
				lstArgs.Add(chbObject.Checked.ToString()); // 2
				lstArgs.Add(chbDetections.Checked.ToString()); // 3
				lstArgs.Add(chbAlarmWarningsHMI.Checked.ToString()); // 4

				GenerateInfo();

				// Start BG worker
				oBG_ProjectFile.RunWorkerAsync(lstArgs);
			}

			


		}

		private void GenerateInfo()
		{
			List<clsObjectDescription> lstObjectDescription = new List<clsObjectDescription>();

			foreach (clsObjectDescription item in lsbObjects.Items)
			{
				lstObjectDescription.Add(item);
			}

			List<clsSoftwareUnit> lstSoftwareUnits = new List<clsSoftwareUnit>();

			foreach (clsSoftwareUnit item in lsbSoftwareUnits.Items)
			{
				lstSoftwareUnits.Add(item);
			}

			if (lstObjectDescription.Count > 0)
			{
				clsComp.SetObjectDiscription(lstObjectDescription);
			}

			if (_lstSoftWareObjectActuators.Count > 0)
			{
				clsComp.SetSoftwareObjectActuators(_lstSoftWareObjectActuators);
			}

			if (lstSoftwareUnits.Count > 0)
			{
				clsComp.SetSoftwareUnits(lstSoftwareUnits);
			}

			if (_lstSoftwareObjectDetections.Count > 0)
			{
				clsComp.SetSoftwareObjectDetections(_lstSoftwareObjectDetections);
			}

		}


		#region New Objects

		private void btnGetIO_Click(object sender, EventArgs e)
		{
			// Displays an OpenFileDialog so the user can select a Cursor.
			OpenFileDialog ofdOpen = new OpenFileDialog();
			ofdOpen.Filter = "Excel Files|*.csv;";
			ofdOpen.Title = "Select an csv File";
			ofdOpen.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

			// Show the Dialog.
			if (ofdOpen.ShowDialog() == System.Windows.Forms.DialogResult.OK)
			{
				clsComp.GetProjectInfo(ofdOpen.FileName);

				cmbInputs.Items.Clear();
				cmbOutputs.Items.Clear();
				cmbDetections.Items.Clear();

				foreach (var Input in clsComp.ConfigFromFile.DI_Simple)
				{
					cmbInputs.Items.Add(Input);
					cmbDetections.Items.Add(Input);
				}

				foreach (var Output in clsComp.ConfigFromFile.DO)
				{
					cmbOutputs.Items.Add(Output);
				}

				foreach (var Input in clsComp.ConfigFromFile.AI_Simple)
				{
					cmbDetections.Items.Add(Input);
				}

				btnAddObject.Enabled = true;
			}
		}
		
		// Selection changed in object listbox
		private void lsbObjects_SelectedIndexChanged(object sender, EventArgs e)
		{
			RefreshListBoxes();
		}

		// update listboxes
		private void RefreshListBoxes()
		{
			clsObjectDescription oObjectDescription = (clsObjectDescription)lsbObjects.SelectedItem;

			if (oObjectDescription != null)
			{
				clsSoftwareObjectActuator oSoftwareObject = _lstSoftWareObjectActuators.Find(o => o.SoftwareObjectName == oObjectDescription.ObjectName);

				if (oSoftwareObject != null)
				{
					lsbInputs.Items.Clear();
					lsbOutputs.Items.Clear();

					foreach (clsObjectParameterConfig input in oSoftwareObject.InputConfig)
					{
						lsbInputs.Items.Add(input);
					}

					foreach (clsObjectParameterConfig output in oSoftwareObject.OutputConfig)
					{
						lsbOutputs.Items.Add(output);
					}
				}
			}

		}

		private void btnAdd_Click(object sender, EventArgs e)
		{
			if (cmbGenericObjects.SelectedItem != null)
			{
				lsbObjects.Items.Add(new clsObjectDescription(txtSoftwareObjectName.Text, cmbGenericObjects.SelectedItem.ToString(), txtSoftwareObjectDescription.Text, txtSoftwareObjectGroup.Text));
				clsGenericObjectType oObject = clsComp.GenericObjects.Find(o => o.ObjectName == cmbGenericObjects.SelectedItem.ToString());
				_lstSoftWareObjectActuators.Add(new clsSoftwareObjectActuator(oObject, txtSoftwareObjectName.Text));
			}
			
		}

		private void btnRemove_Click(object sender, EventArgs e)
		{
			if (lsbObjects.SelectedItem != null)
			{
				clsObjectDescription objectDescription = (clsObjectDescription)lsbObjects.SelectedItem;
				clsSoftwareObjectActuator oSoftwareObjectActuator = _lstSoftWareObjectActuators.Find(o => o.SoftwareObjectName == objectDescription.ObjectName);
				_lstSoftWareObjectActuators.Remove(oSoftwareObjectActuator);
				lsbObjects.Items.Remove(lsbObjects.SelectedItem);
			}						
		}
		
		private void btnAddInput_Click(object sender, EventArgs e)
		{
			clsObjectParameterConfig ObjectParameterConfig = (clsObjectParameterConfig)lsbInputs.SelectedItem;
			if (ObjectParameterConfig != null)
			{
				ObjectParameterConfig.IOPoint = cmbInputs.SelectedItem.ToString();
				// Delete input from list
				cmbInputs.Items.Remove(cmbInputs.SelectedItem);
				RefreshListBoxes();
			}
			
		}

		private void btnAddOutput_Click(object sender, EventArgs e)
		{
			clsObjectParameterConfig ObjectParameterConfig = (clsObjectParameterConfig)lsbOutputs.SelectedItem;
			if (ObjectParameterConfig != null)
			{
				ObjectParameterConfig.IOPoint = cmbOutputs.SelectedItem.ToString();
				// Delete output from list
				cmbOutputs.Items.Remove(cmbOutputs.SelectedItem);
				RefreshListBoxes();
			}
			
		}

		private void btnRemoveInput_Click(object sender, EventArgs e)
		{
			clsObjectParameterConfig ObjectParameterConfig = (clsObjectParameterConfig)lsbInputs.SelectedItem;
			// Add input again to list
			cmbInputs.Items.Add(ObjectParameterConfig.IOPoint);
			ObjectParameterConfig.IOPoint = string.Empty;
			RefreshListBoxes();
		}

		private void btnRemoveOutput_Click(object sender, EventArgs e)
		{
			clsObjectParameterConfig ObjectParameterConfig = (clsObjectParameterConfig)lsbOutputs.SelectedItem;
			// Add output again to list
			cmbOutputs.Items.Add(ObjectParameterConfig.IOPoint);
			ObjectParameterConfig.IOPoint = string.Empty;
			RefreshListBoxes();
		}

		private void btnAddSoftwareUnit_Click(object sender, EventArgs e)
		{
			lsbSoftwareUnits.Items.Add(new clsSoftwareUnit(txtSoftwareUnit.Text, Convert.ToInt32(txtNumberOfAlarms.Text), Convert.ToInt32(txtNumberOfWarnings.Text)));
		}

		private void btnRemoveSoftwareUnit_Click(object sender, EventArgs e)
		{
			lsbSoftwareUnits.Items.Remove(lsbSoftwareUnits.SelectedItem);
		}

		private void lsbSoftwareUnits_SelectedIndexChanged(object sender, EventArgs e)
		{
			UpdateAlarmWarningConfig();
		}

		private void txtText_Validated(object sender, EventArgs e)
		{
			ModifyAlarmWarningObject();
			UpdateAlarmWarningConfig();
		}

		private void txtTag_Validated(object sender, EventArgs e)
		{
			ModifyAlarmWarningObject();
			UpdateAlarmWarningConfig();
		}

		private void lsbAlarmWarningConfig_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (lsbAlarmWarningConfig.SelectedItem is clsAlarm)
			{
				clsAlarm oAlarm = (clsAlarm)lsbAlarmWarningConfig.SelectedItem;

				if (oAlarm != null)
				{
					txtText.Text = oAlarm.AlarmText;
					txtTag.Text = oAlarm.AlarmTag;
				}
			}

			if (lsbAlarmWarningConfig.SelectedItem is clsWarning)
			{
				clsWarning oWarning = (clsWarning)lsbAlarmWarningConfig.SelectedItem;


				if (oWarning != null)
				{
					txtText.Text = oWarning.WarningText;
					txtTag.Text = oWarning.WarningTag;
				}

			}
			

		}
		
		private void UpdateAlarmWarningConfig()
		{
			clsSoftwareUnit oSoftwareUnit = (clsSoftwareUnit)lsbSoftwareUnits.SelectedItem;
			lsbAlarmWarningConfig.Items.Clear();


			if (oSoftwareUnit != null)
			{

				foreach (clsAlarm alarm in oSoftwareUnit.AlarmCfg)
				{
					lsbAlarmWarningConfig.Items.Add(alarm);
				}

				foreach (clsWarning warning in oSoftwareUnit.WarningCfg)
				{
					lsbAlarmWarningConfig.Items.Add(warning);
				}

			}	
		
		}

		private void ModifyAlarmWarningObject()
		{
			if (lsbAlarmWarningConfig.SelectedItem is clsAlarm)
			{
				clsAlarm oAlarm = (clsAlarm)lsbAlarmWarningConfig.SelectedItem;

				if (oAlarm != null)
				{
					oAlarm.AlarmText = txtText.Text;
					oAlarm.AlarmTag = txtTag.Text;
				}

			}

			if (lsbAlarmWarningConfig.SelectedItem is clsWarning)
			{
				clsWarning oWarning = (clsWarning)lsbAlarmWarningConfig.SelectedItem;

				if (oWarning != null)
				{
					oWarning.WarningText = txtText.Text;
					oWarning.WarningTag = txtTag.Text;
				}
			}

		}

		private void btnAddDetection_Click(object sender, EventArgs e)
		{
			clsGenericObjectType oObject = clsComp.GenericObjects.Find(o => o.ObjectName == cmbGenericObjectSensors.SelectedItem.ToString());
			if (oObject != null)
			{
				clsSoftwareObjectDetection oSoftwareObjectDetection = new clsSoftwareObjectDetection(oObject, txtDetectionName.Text);
				_lstSoftwareObjectDetections.Add(oSoftwareObjectDetection);
				lsbDetections.Items.Add(oSoftwareObjectDetection);
			}
			
		}

		private void btnRemoveDetection_Click(object sender, EventArgs e)
		{
			clsSoftwareObjectDetection oSoftwareObjectDetection = (clsSoftwareObjectDetection)lsbDetections.SelectedItem;
			_lstSoftwareObjectDetections.Remove(oSoftwareObjectDetection);
			lsbDetections.Items.Remove(lsbDetections.SelectedItem);
		}

		private void btnAddInputDetection_Click(object sender, EventArgs e)
		{			
			clsObjectParameterConfig ObjectParameterConfig = (clsObjectParameterConfig) lsbInputsDetection.SelectedItem;
			if (ObjectParameterConfig != null)
			{
				ObjectParameterConfig.IOPoint = cmbDetections.SelectedItem.ToString();
				// Delete input from list
				cmbDetections.Items.Remove(cmbInputs.SelectedItem);
				UpdateDetectionsListBox();
			}
			
		}

		private void btnRemoveOutputDetection_Click(object sender, EventArgs e)
		{
			clsObjectParameterConfig ObjectParameterConfig = (clsObjectParameterConfig)lsbInputsDetection.SelectedItem;
			// Add input again to list
			cmbDetections.Items.Add(ObjectParameterConfig.IOPoint);
			ObjectParameterConfig.IOPoint = string.Empty;
			UpdateDetectionsListBox();
		}

		private void lsbDetections_SelectedIndexChanged(object sender, EventArgs e)
		{
			UpdateDetectionsListBox();
		}

		private void UpdateDetectionsListBox()
		{
			clsSoftwareObjectDetection oSoftwareObjectDetection = (clsSoftwareObjectDetection)lsbDetections.SelectedItem;

			if (oSoftwareObjectDetection != null)
			{

				lsbInputsDetection.Items.Clear();

				foreach (clsObjectParameterConfig input in oSoftwareObjectDetection.InputConfig)
				{
					lsbInputsDetection.Items.Add(input);
				}
			}
		}

		#endregion



		#region Save en Restore to file


		private void btnSave_Click(object sender, EventArgs e)
		{
			// Displays an OpenFileDialog so the user can select a Cursor.
			SaveFileDialog oSfdSave = new SaveFileDialog();
			oSfdSave.Filter = "Source Files|*.xml";
			oSfdSave.Title = "Select target location for configuration";
			oSfdSave.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
			
			// Show the Dialog.
			if (oSfdSave.ShowDialog() == System.Windows.Forms.DialogResult.OK)
			{
				GenerateInfo();
				WriteToXmlFile<clsConfigFromFile>(oSfdSave.FileName, clsComp.ConfigFromFile, false);
			}
						
		}

		private void btnRestore_Click(object sender, EventArgs e)
		{
			btnAddObject.Enabled = true;
			
			// Displays an OpenFileDialog so the user can select a Cursor.
			OpenFileDialog oSfdOpen = new OpenFileDialog();
			oSfdOpen.Filter = "Source Files|*.xml";
			oSfdOpen.Title = "Select source location for configuration";
			oSfdOpen.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);


			// Show the Dialog.
			if (oSfdOpen.ShowDialog() == System.Windows.Forms.DialogResult.OK)
			{
				clsConfigFromFile oConfig;
				oConfig = ReadFromXmlFile<clsConfigFromFile>(oSfdOpen.FileName);
				clsComp.GetProjectInfo(oConfig);

				_lstSoftWareObjectActuators = oConfig.SoftwareObjectActuators;
				_lstObjectDescription = oConfig.ObjectDiscription;
				_lstSoftwareObjectDetections = oConfig.SoftwareObjectDetections;

				// Get info to forms
				LoadConfigToForm();
			}
			
		}
		
		private void LoadConfigToForm()
		{
			// Clear listboxes
			lsbObjects.Items.Clear();
			lsbInputs.Items.Clear();
			lsbOutputs.Items.Clear();
			lsbSoftwareUnits.Items.Clear();
			lsbAlarmWarningConfig.Items.Clear();
			lsbDetections.Items.Clear();
			lsbInputsDetection.Items.Clear();
			cmbInputs.Items.Clear();
			cmbOutputs.Items.Clear();
			cmbDetections.Items.Clear();

			// Populate listbox with objects
			foreach (clsObjectDescription Description in clsComp.ConfigFromFile.ObjectDiscription)
			{
				lsbObjects.Items.Add(Description);
			}

			// Populate alarm warning configuration
			foreach (clsSoftwareUnit Unit in clsComp.ConfigFromFile.SoftwareUnits)
			{
				lsbSoftwareUnits.Items.Add(Unit);
			}

			// Populate listbox with detections
			foreach (clsSoftwareObjectDetection Detetection in clsComp.ConfigFromFile.SoftwareObjectDetections)
			{
				lsbDetections.Items.Add(Detetection);
			}

			foreach (var Input in clsComp.ConfigFromFile.DI_Simple)
			{
				cmbInputs.Items.Add(Input);
				cmbDetections.Items.Add(Input);
			}

			foreach (var Output in clsComp.ConfigFromFile.DO)
			{
				cmbOutputs.Items.Add(Output);
			}

			foreach (var Input in clsComp.ConfigFromFile.AI_Simple)
			{
				cmbDetections.Items.Add(Input);
			}
		}


		/// <summary>
		/// Writes the given object instance to an XML file.
		/// <para>Only Public properties and variables will be written to the file. These can be any type though, even other classes.</para>
		/// <para>If there are public properties/variables that you do not want written to the file, decorate them with the [XmlIgnore] attribute.</para>
		/// <para>Object type must have a parameterless constructor.</para>
		/// </summary>
		/// <typeparam name="T">The type of object being written to the file.</typeparam>
		/// <param name="filePath">The file path to write the object instance to.</param>
		/// <param name="objectToWrite">The object instance to write to the file.</param>
		/// <param name="append">If false the file will be overwritten if it already exists. If true the contents will be appended to the file.</param>
		public static void WriteToXmlFile<T>(string filePath, T objectToWrite, bool append = false) where T : new()
		{
			TextWriter writer = null;
			try
			{
				var serializer = new XmlSerializer(typeof(T));
				writer = new StreamWriter(filePath, append);
				serializer.Serialize(writer, objectToWrite);
			}
			finally
			{
				if (writer != null)
					writer.Close();
			}
		}

		/// <summary>
		/// Reads an object instance from an XML file.
		/// <para>Object type must have a parameterless constructor.</para>
		/// </summary>
		/// <typeparam name="T">The type of object to read from the file.</typeparam>
		/// <param name="filePath">The file path to read the object instance from.</param>
		/// <returns>Returns a new instance of the object read from the XML file.</returns>
		public static T ReadFromXmlFile<T>(string filePath) where T : new()
		{
			TextReader reader = null;
			try
			{
				var serializer = new XmlSerializer(typeof(T));
				reader = new StreamReader(filePath);
				return (T)serializer.Deserialize(reader);
			}
			finally
			{
				if (reader != null)
					reader.Close();
			}
		}



		#endregion

	}





}
