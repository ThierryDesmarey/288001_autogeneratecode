﻿namespace AUTO_GENERATE_FB
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.btnAddObject = new System.Windows.Forms.Button();
			this.grbConfig = new System.Windows.Forms.GroupBox();
			this.btnRestore = new System.Windows.Forms.Button();
			this.btnSave = new System.Windows.Forms.Button();
			this.btnGetIO = new System.Windows.Forms.Button();
			this.chbAlarmWarningsHMI = new System.Windows.Forms.CheckBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.chbDetections = new System.Windows.Forms.CheckBox();
			this.chbObject = new System.Windows.Forms.CheckBox();
			this.chbAlarmWarnings = new System.Windows.Forms.CheckBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.btnAdd = new System.Windows.Forms.Button();
			this.btnRemoveOutput = new System.Windows.Forms.Button();
			this.btnAddOutput = new System.Windows.Forms.Button();
			this.btnRemoveInput = new System.Windows.Forms.Button();
			this.btnAddInput = new System.Windows.Forms.Button();
			this.cmbOutputs = new System.Windows.Forms.ComboBox();
			this.lsbOutputs = new System.Windows.Forms.ListBox();
			this.cmbInputs = new System.Windows.Forms.ComboBox();
			this.lsbInputs = new System.Windows.Forms.ListBox();
			this.label9 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.btnRemove = new System.Windows.Forms.Button();
			this.cmbGenericObjects = new System.Windows.Forms.ComboBox();
			this.txtSoftwareObjectDescription = new System.Windows.Forms.TextBox();
			this.txtSoftwareObjectGroup = new System.Windows.Forms.TextBox();
			this.txtSoftwareObjectName = new System.Windows.Forms.TextBox();
			this.lsbObjects = new System.Windows.Forms.ListBox();
			this.grbAlarmWarning = new System.Windows.Forms.GroupBox();
			this.txtText = new System.Windows.Forms.TextBox();
			this.txtTag = new System.Windows.Forms.TextBox();
			this.lsbAlarmWarningConfig = new System.Windows.Forms.ListBox();
			this.btnRemoveSoftwareUnit = new System.Windows.Forms.Button();
			this.btnAddSoftwareUnit = new System.Windows.Forms.Button();
			this.lsbSoftwareUnits = new System.Windows.Forms.ListBox();
			this.label11 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.txtSoftwareUnit = new System.Windows.Forms.TextBox();
			this.txtNumberOfWarnings = new System.Windows.Forms.TextBox();
			this.txtNumberOfAlarms = new System.Windows.Forms.TextBox();
			this.grbDetections = new System.Windows.Forms.GroupBox();
			this.btnRemoveDetection = new System.Windows.Forms.Button();
			this.btnAddDetection = new System.Windows.Forms.Button();
			this.lsbDetections = new System.Windows.Forms.ListBox();
			this.btnRemoveOutputDetection = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.btnAddInputDetection = new System.Windows.Forms.Button();
			this.txtDetectionName = new System.Windows.Forms.TextBox();
			this.cmbDetections = new System.Windows.Forms.ComboBox();
			this.label4 = new System.Windows.Forms.Label();
			this.lsbInputsDetection = new System.Windows.Forms.ListBox();
			this.cmbGenericObjectSensors = new System.Windows.Forms.ComboBox();
			this.grbConfig.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.grbAlarmWarning.SuspendLayout();
			this.grbDetections.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnAddObject
			// 
			this.btnAddObject.Location = new System.Drawing.Point(457, 50);
			this.btnAddObject.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.btnAddObject.Name = "btnAddObject";
			this.btnAddObject.Size = new System.Drawing.Size(186, 35);
			this.btnAddObject.TabIndex = 3;
			this.btnAddObject.Text = "Genereer code";
			this.btnAddObject.UseVisualStyleBackColor = true;
			this.btnAddObject.Click += new System.EventHandler(this.btnAddObject_Click);
			// 
			// grbConfig
			// 
			this.grbConfig.Controls.Add(this.btnRestore);
			this.grbConfig.Controls.Add(this.btnSave);
			this.grbConfig.Controls.Add(this.btnGetIO);
			this.grbConfig.Controls.Add(this.chbAlarmWarningsHMI);
			this.grbConfig.Controls.Add(this.label2);
			this.grbConfig.Controls.Add(this.label1);
			this.grbConfig.Controls.Add(this.chbDetections);
			this.grbConfig.Controls.Add(this.chbObject);
			this.grbConfig.Controls.Add(this.chbAlarmWarnings);
			this.grbConfig.Controls.Add(this.btnAddObject);
			this.grbConfig.Location = new System.Drawing.Point(12, 12);
			this.grbConfig.Name = "grbConfig";
			this.grbConfig.Size = new System.Drawing.Size(649, 165);
			this.grbConfig.TabIndex = 4;
			this.grbConfig.TabStop = false;
			this.grbConfig.Text = "Configuratie objecten";
			// 
			// btnRestore
			// 
			this.btnRestore.Location = new System.Drawing.Point(457, 124);
			this.btnRestore.Name = "btnRestore";
			this.btnRestore.Size = new System.Drawing.Size(186, 35);
			this.btnRestore.TabIndex = 30;
			this.btnRestore.Text = "Restore config";
			this.btnRestore.UseVisualStyleBackColor = true;
			this.btnRestore.Click += new System.EventHandler(this.btnRestore_Click);
			// 
			// btnSave
			// 
			this.btnSave.Location = new System.Drawing.Point(457, 88);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(186, 32);
			this.btnSave.TabIndex = 28;
			this.btnSave.Text = "Save config";
			this.btnSave.UseVisualStyleBackColor = true;
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// btnGetIO
			// 
			this.btnGetIO.Location = new System.Drawing.Point(457, 16);
			this.btnGetIO.Name = "btnGetIO";
			this.btnGetIO.Size = new System.Drawing.Size(186, 32);
			this.btnGetIO.TabIndex = 27;
			this.btnGetIO.Text = "Get IO configuration";
			this.btnGetIO.UseVisualStyleBackColor = true;
			this.btnGetIO.Click += new System.EventHandler(this.btnGetIO_Click);
			// 
			// chbAlarmWarningsHMI
			// 
			this.chbAlarmWarningsHMI.AutoSize = true;
			this.chbAlarmWarningsHMI.Location = new System.Drawing.Point(221, 63);
			this.chbAlarmWarningsHMI.Name = "chbAlarmWarningsHMI";
			this.chbAlarmWarningsHMI.Size = new System.Drawing.Size(166, 24);
			this.chbAlarmWarningsHMI.TabIndex = 18;
			this.chbAlarmWarningsHMI.Text = "Alarmen /  warnings";
			this.chbAlarmWarningsHMI.UseVisualStyleBackColor = true;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(16, 33);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(42, 20);
			this.label2.TabIndex = 17;
			this.label2.Text = "PLC";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(215, 33);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(42, 20);
			this.label1.TabIndex = 16;
			this.label1.Text = "HMI";
			// 
			// chbDetections
			// 
			this.chbDetections.AutoSize = true;
			this.chbDetections.Location = new System.Drawing.Point(20, 97);
			this.chbDetections.Name = "chbDetections";
			this.chbDetections.Size = new System.Drawing.Size(121, 24);
			this.chbDetections.TabIndex = 15;
			this.chbDetections.Text = "Detecties FB";
			this.chbDetections.UseVisualStyleBackColor = true;
			// 
			// chbObject
			// 
			this.chbObject.AutoSize = true;
			this.chbObject.Location = new System.Drawing.Point(20, 127);
			this.chbObject.Name = "chbObject";
			this.chbObject.Size = new System.Drawing.Size(117, 24);
			this.chbObject.TabIndex = 11;
			this.chbObject.Text = "Objecten FB";
			this.chbObject.UseVisualStyleBackColor = true;
			// 
			// chbAlarmWarnings
			// 
			this.chbAlarmWarnings.AutoSize = true;
			this.chbAlarmWarnings.Location = new System.Drawing.Point(20, 67);
			this.chbAlarmWarnings.Name = "chbAlarmWarnings";
			this.chbAlarmWarnings.Size = new System.Drawing.Size(167, 24);
			this.chbAlarmWarnings.TabIndex = 9;
			this.chbAlarmWarnings.Text = "Alarm / Warning DB";
			this.chbAlarmWarnings.UseVisualStyleBackColor = true;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.btnAdd);
			this.groupBox1.Controls.Add(this.btnRemoveOutput);
			this.groupBox1.Controls.Add(this.btnAddOutput);
			this.groupBox1.Controls.Add(this.btnRemoveInput);
			this.groupBox1.Controls.Add(this.btnAddInput);
			this.groupBox1.Controls.Add(this.cmbOutputs);
			this.groupBox1.Controls.Add(this.lsbOutputs);
			this.groupBox1.Controls.Add(this.cmbInputs);
			this.groupBox1.Controls.Add(this.lsbInputs);
			this.groupBox1.Controls.Add(this.label9);
			this.groupBox1.Controls.Add(this.label8);
			this.groupBox1.Controls.Add(this.label7);
			this.groupBox1.Controls.Add(this.label6);
			this.groupBox1.Controls.Add(this.btnRemove);
			this.groupBox1.Controls.Add(this.cmbGenericObjects);
			this.groupBox1.Controls.Add(this.txtSoftwareObjectDescription);
			this.groupBox1.Controls.Add(this.txtSoftwareObjectGroup);
			this.groupBox1.Controls.Add(this.txtSoftwareObjectName);
			this.groupBox1.Controls.Add(this.lsbObjects);
			this.groupBox1.Location = new System.Drawing.Point(12, 183);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(649, 785);
			this.groupBox1.TabIndex = 27;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Configuration objects";
			// 
			// btnAdd
			// 
			this.btnAdd.Location = new System.Drawing.Point(526, 30);
			this.btnAdd.Name = "btnAdd";
			this.btnAdd.Size = new System.Drawing.Size(100, 61);
			this.btnAdd.TabIndex = 40;
			this.btnAdd.Text = "Add";
			this.btnAdd.UseVisualStyleBackColor = true;
			this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
			// 
			// btnRemoveOutput
			// 
			this.btnRemoveOutput.Location = new System.Drawing.Point(498, 518);
			this.btnRemoveOutput.Name = "btnRemoveOutput";
			this.btnRemoveOutput.Size = new System.Drawing.Size(128, 28);
			this.btnRemoveOutput.TabIndex = 38;
			this.btnRemoveOutput.Text = "Remove";
			this.btnRemoveOutput.UseVisualStyleBackColor = true;
			this.btnRemoveOutput.Click += new System.EventHandler(this.btnRemoveOutput_Click);
			// 
			// btnAddOutput
			// 
			this.btnAddOutput.Location = new System.Drawing.Point(368, 518);
			this.btnAddOutput.Name = "btnAddOutput";
			this.btnAddOutput.Size = new System.Drawing.Size(124, 28);
			this.btnAddOutput.TabIndex = 37;
			this.btnAddOutput.Text = "Add";
			this.btnAddOutput.UseVisualStyleBackColor = true;
			this.btnAddOutput.Click += new System.EventHandler(this.btnAddOutput_Click);
			// 
			// btnRemoveInput
			// 
			this.btnRemoveInput.Location = new System.Drawing.Point(498, 329);
			this.btnRemoveInput.Name = "btnRemoveInput";
			this.btnRemoveInput.Size = new System.Drawing.Size(128, 28);
			this.btnRemoveInput.TabIndex = 36;
			this.btnRemoveInput.Text = "Remove";
			this.btnRemoveInput.UseVisualStyleBackColor = true;
			this.btnRemoveInput.Click += new System.EventHandler(this.btnRemoveInput_Click);
			// 
			// btnAddInput
			// 
			this.btnAddInput.Location = new System.Drawing.Point(368, 330);
			this.btnAddInput.Name = "btnAddInput";
			this.btnAddInput.Size = new System.Drawing.Size(124, 28);
			this.btnAddInput.TabIndex = 35;
			this.btnAddInput.Text = "Add";
			this.btnAddInput.UseVisualStyleBackColor = true;
			this.btnAddInput.Click += new System.EventHandler(this.btnAddInput_Click);
			// 
			// cmbOutputs
			// 
			this.cmbOutputs.FormattingEnabled = true;
			this.cmbOutputs.Location = new System.Drawing.Point(20, 518);
			this.cmbOutputs.Name = "cmbOutputs";
			this.cmbOutputs.Size = new System.Drawing.Size(342, 28);
			this.cmbOutputs.TabIndex = 34;
			// 
			// lsbOutputs
			// 
			this.lsbOutputs.FormattingEnabled = true;
			this.lsbOutputs.ItemHeight = 20;
			this.lsbOutputs.Location = new System.Drawing.Point(20, 560);
			this.lsbOutputs.Name = "lsbOutputs";
			this.lsbOutputs.Size = new System.Drawing.Size(606, 204);
			this.lsbOutputs.TabIndex = 33;
			// 
			// cmbInputs
			// 
			this.cmbInputs.FormattingEnabled = true;
			this.cmbInputs.Location = new System.Drawing.Point(20, 329);
			this.cmbInputs.Name = "cmbInputs";
			this.cmbInputs.Size = new System.Drawing.Size(342, 28);
			this.cmbInputs.TabIndex = 32;
			// 
			// lsbInputs
			// 
			this.lsbInputs.FormattingEnabled = true;
			this.lsbInputs.ItemHeight = 20;
			this.lsbInputs.Location = new System.Drawing.Point(20, 363);
			this.lsbInputs.Name = "lsbInputs";
			this.lsbInputs.Size = new System.Drawing.Size(606, 144);
			this.lsbInputs.TabIndex = 31;
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label9.Location = new System.Drawing.Point(16, 128);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(205, 20);
			this.label9.TabIndex = 30;
			this.label9.Text = "Software object description:";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label8.Location = new System.Drawing.Point(16, 96);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(169, 20);
			this.label8.TabIndex = 29;
			this.label8.Text = "Software object group:";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label7.Location = new System.Drawing.Point(16, 62);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(158, 20);
			this.label7.TabIndex = 28;
			this.label7.Text = "Software object type:";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.Location = new System.Drawing.Point(16, 33);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(168, 20);
			this.label6.TabIndex = 27;
			this.label6.Text = "Software object name:";
			// 
			// btnRemove
			// 
			this.btnRemove.Location = new System.Drawing.Point(526, 96);
			this.btnRemove.Name = "btnRemove";
			this.btnRemove.Size = new System.Drawing.Size(100, 58);
			this.btnRemove.TabIndex = 15;
			this.btnRemove.Text = "Remove";
			this.btnRemove.UseVisualStyleBackColor = true;
			this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
			// 
			// cmbGenericObjects
			// 
			this.cmbGenericObjects.FormattingEnabled = true;
			this.cmbGenericObjects.Location = new System.Drawing.Point(230, 62);
			this.cmbGenericObjects.Name = "cmbGenericObjects";
			this.cmbGenericObjects.Size = new System.Drawing.Size(285, 28);
			this.cmbGenericObjects.TabIndex = 13;
			// 
			// txtSoftwareObjectDescription
			// 
			this.txtSoftwareObjectDescription.Location = new System.Drawing.Point(230, 128);
			this.txtSoftwareObjectDescription.Name = "txtSoftwareObjectDescription";
			this.txtSoftwareObjectDescription.Size = new System.Drawing.Size(285, 26);
			this.txtSoftwareObjectDescription.TabIndex = 12;
			// 
			// txtSoftwareObjectGroup
			// 
			this.txtSoftwareObjectGroup.Location = new System.Drawing.Point(230, 96);
			this.txtSoftwareObjectGroup.Name = "txtSoftwareObjectGroup";
			this.txtSoftwareObjectGroup.Size = new System.Drawing.Size(285, 26);
			this.txtSoftwareObjectGroup.TabIndex = 10;
			// 
			// txtSoftwareObjectName
			// 
			this.txtSoftwareObjectName.Location = new System.Drawing.Point(230, 30);
			this.txtSoftwareObjectName.Name = "txtSoftwareObjectName";
			this.txtSoftwareObjectName.Size = new System.Drawing.Size(285, 26);
			this.txtSoftwareObjectName.TabIndex = 9;
			// 
			// lsbObjects
			// 
			this.lsbObjects.FormattingEnabled = true;
			this.lsbObjects.ItemHeight = 20;
			this.lsbObjects.Location = new System.Drawing.Point(20, 160);
			this.lsbObjects.Name = "lsbObjects";
			this.lsbObjects.Size = new System.Drawing.Size(606, 164);
			this.lsbObjects.TabIndex = 8;
			this.lsbObjects.SelectedIndexChanged += new System.EventHandler(this.lsbObjects_SelectedIndexChanged);
			// 
			// grbAlarmWarning
			// 
			this.grbAlarmWarning.Controls.Add(this.txtText);
			this.grbAlarmWarning.Controls.Add(this.txtTag);
			this.grbAlarmWarning.Controls.Add(this.lsbAlarmWarningConfig);
			this.grbAlarmWarning.Controls.Add(this.btnRemoveSoftwareUnit);
			this.grbAlarmWarning.Controls.Add(this.btnAddSoftwareUnit);
			this.grbAlarmWarning.Controls.Add(this.lsbSoftwareUnits);
			this.grbAlarmWarning.Controls.Add(this.label11);
			this.grbAlarmWarning.Controls.Add(this.label10);
			this.grbAlarmWarning.Controls.Add(this.label12);
			this.grbAlarmWarning.Controls.Add(this.txtSoftwareUnit);
			this.grbAlarmWarning.Controls.Add(this.txtNumberOfWarnings);
			this.grbAlarmWarning.Controls.Add(this.txtNumberOfAlarms);
			this.grbAlarmWarning.Location = new System.Drawing.Point(667, 12);
			this.grbAlarmWarning.Name = "grbAlarmWarning";
			this.grbAlarmWarning.Size = new System.Drawing.Size(552, 956);
			this.grbAlarmWarning.TabIndex = 28;
			this.grbAlarmWarning.TabStop = false;
			this.grbAlarmWarning.Text = "Alarm & warning configuration";
			// 
			// txtText
			// 
			this.txtText.Location = new System.Drawing.Point(6, 564);
			this.txtText.Name = "txtText";
			this.txtText.Size = new System.Drawing.Size(530, 26);
			this.txtText.TabIndex = 50;
			this.txtText.Validated += new System.EventHandler(this.txtText_Validated);
			// 
			// txtTag
			// 
			this.txtTag.Location = new System.Drawing.Point(6, 534);
			this.txtTag.Name = "txtTag";
			this.txtTag.Size = new System.Drawing.Size(530, 26);
			this.txtTag.TabIndex = 49;
			this.txtTag.Validated += new System.EventHandler(this.txtTag_Validated);
			// 
			// lsbAlarmWarningConfig
			// 
			this.lsbAlarmWarningConfig.FormattingEnabled = true;
			this.lsbAlarmWarningConfig.ItemHeight = 20;
			this.lsbAlarmWarningConfig.Location = new System.Drawing.Point(6, 598);
			this.lsbAlarmWarningConfig.Name = "lsbAlarmWarningConfig";
			this.lsbAlarmWarningConfig.Size = new System.Drawing.Size(530, 344);
			this.lsbAlarmWarningConfig.TabIndex = 48;
			this.lsbAlarmWarningConfig.SelectedIndexChanged += new System.EventHandler(this.lsbAlarmWarningConfig_SelectedIndexChanged);
			// 
			// btnRemoveSoftwareUnit
			// 
			this.btnRemoveSoftwareUnit.Location = new System.Drawing.Point(272, 483);
			this.btnRemoveSoftwareUnit.Name = "btnRemoveSoftwareUnit";
			this.btnRemoveSoftwareUnit.Size = new System.Drawing.Size(264, 43);
			this.btnRemoveSoftwareUnit.TabIndex = 47;
			this.btnRemoveSoftwareUnit.Text = "Remove";
			this.btnRemoveSoftwareUnit.UseVisualStyleBackColor = true;
			this.btnRemoveSoftwareUnit.Click += new System.EventHandler(this.btnRemoveSoftwareUnit_Click);
			// 
			// btnAddSoftwareUnit
			// 
			this.btnAddSoftwareUnit.Location = new System.Drawing.Point(6, 483);
			this.btnAddSoftwareUnit.Name = "btnAddSoftwareUnit";
			this.btnAddSoftwareUnit.Size = new System.Drawing.Size(260, 45);
			this.btnAddSoftwareUnit.TabIndex = 46;
			this.btnAddSoftwareUnit.Text = "Add";
			this.btnAddSoftwareUnit.UseVisualStyleBackColor = true;
			this.btnAddSoftwareUnit.Click += new System.EventHandler(this.btnAddSoftwareUnit_Click);
			// 
			// lsbSoftwareUnits
			// 
			this.lsbSoftwareUnits.FormattingEnabled = true;
			this.lsbSoftwareUnits.ItemHeight = 20;
			this.lsbSoftwareUnits.Location = new System.Drawing.Point(6, 129);
			this.lsbSoftwareUnits.Name = "lsbSoftwareUnits";
			this.lsbSoftwareUnits.Size = new System.Drawing.Size(530, 344);
			this.lsbSoftwareUnits.TabIndex = 45;
			this.lsbSoftwareUnits.SelectedIndexChanged += new System.EventHandler(this.lsbSoftwareUnits_SelectedIndexChanged);
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label11.Location = new System.Drawing.Point(8, 97);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(154, 20);
			this.label11.TabIndex = 44;
			this.label11.Text = "Number of warnings:";
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label10.Location = new System.Drawing.Point(8, 36);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(107, 20);
			this.label10.TabIndex = 42;
			this.label10.Text = "Software unit:";
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label12.Location = new System.Drawing.Point(8, 65);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(138, 20);
			this.label12.TabIndex = 43;
			this.label12.Text = "Number of alarms:";
			// 
			// txtSoftwareUnit
			// 
			this.txtSoftwareUnit.Location = new System.Drawing.Point(172, 33);
			this.txtSoftwareUnit.Name = "txtSoftwareUnit";
			this.txtSoftwareUnit.Size = new System.Drawing.Size(364, 26);
			this.txtSoftwareUnit.TabIndex = 41;
			// 
			// txtNumberOfWarnings
			// 
			this.txtNumberOfWarnings.Location = new System.Drawing.Point(172, 97);
			this.txtNumberOfWarnings.Name = "txtNumberOfWarnings";
			this.txtNumberOfWarnings.Size = new System.Drawing.Size(364, 26);
			this.txtNumberOfWarnings.TabIndex = 42;
			// 
			// txtNumberOfAlarms
			// 
			this.txtNumberOfAlarms.Location = new System.Drawing.Point(172, 65);
			this.txtNumberOfAlarms.Name = "txtNumberOfAlarms";
			this.txtNumberOfAlarms.Size = new System.Drawing.Size(364, 26);
			this.txtNumberOfAlarms.TabIndex = 41;
			// 
			// grbDetections
			// 
			this.grbDetections.Controls.Add(this.btnRemoveDetection);
			this.grbDetections.Controls.Add(this.btnAddDetection);
			this.grbDetections.Controls.Add(this.lsbDetections);
			this.grbDetections.Controls.Add(this.btnRemoveOutputDetection);
			this.grbDetections.Controls.Add(this.label3);
			this.grbDetections.Controls.Add(this.btnAddInputDetection);
			this.grbDetections.Controls.Add(this.txtDetectionName);
			this.grbDetections.Controls.Add(this.cmbDetections);
			this.grbDetections.Controls.Add(this.label4);
			this.grbDetections.Controls.Add(this.lsbInputsDetection);
			this.grbDetections.Controls.Add(this.cmbGenericObjectSensors);
			this.grbDetections.Location = new System.Drawing.Point(1225, 12);
			this.grbDetections.Name = "grbDetections";
			this.grbDetections.Size = new System.Drawing.Size(552, 560);
			this.grbDetections.TabIndex = 29;
			this.grbDetections.TabStop = false;
			this.grbDetections.Text = "Configuratie detections";
			// 
			// btnRemoveDetection
			// 
			this.btnRemoveDetection.Location = new System.Drawing.Point(384, 99);
			this.btnRemoveDetection.Name = "btnRemoveDetection";
			this.btnRemoveDetection.Size = new System.Drawing.Size(152, 27);
			this.btnRemoveDetection.TabIndex = 46;
			this.btnRemoveDetection.Text = "Remove";
			this.btnRemoveDetection.UseVisualStyleBackColor = true;
			this.btnRemoveDetection.Click += new System.EventHandler(this.btnRemoveDetection_Click);
			// 
			// btnAddDetection
			// 
			this.btnAddDetection.Location = new System.Drawing.Point(227, 99);
			this.btnAddDetection.Name = "btnAddDetection";
			this.btnAddDetection.Size = new System.Drawing.Size(151, 28);
			this.btnAddDetection.TabIndex = 45;
			this.btnAddDetection.Text = "Add";
			this.btnAddDetection.UseVisualStyleBackColor = true;
			this.btnAddDetection.Click += new System.EventHandler(this.btnAddDetection_Click);
			// 
			// lsbDetections
			// 
			this.lsbDetections.FormattingEnabled = true;
			this.lsbDetections.ItemHeight = 20;
			this.lsbDetections.Location = new System.Drawing.Point(17, 132);
			this.lsbDetections.Name = "lsbDetections";
			this.lsbDetections.Size = new System.Drawing.Size(519, 224);
			this.lsbDetections.TabIndex = 41;
			this.lsbDetections.SelectedIndexChanged += new System.EventHandler(this.lsbDetections_SelectedIndexChanged);
			// 
			// btnRemoveOutputDetection
			// 
			this.btnRemoveOutputDetection.Location = new System.Drawing.Point(408, 362);
			this.btnRemoveOutputDetection.Name = "btnRemoveOutputDetection";
			this.btnRemoveOutputDetection.Size = new System.Drawing.Size(128, 27);
			this.btnRemoveOutputDetection.TabIndex = 44;
			this.btnRemoveOutputDetection.Text = "Remove";
			this.btnRemoveOutputDetection.UseVisualStyleBackColor = true;
			this.btnRemoveOutputDetection.Click += new System.EventHandler(this.btnRemoveOutputDetection_Click);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(13, 65);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(163, 20);
			this.label3.TabIndex = 44;
			this.label3.Text = "Detection object type:";
			// 
			// btnAddInputDetection
			// 
			this.btnAddInputDetection.Location = new System.Drawing.Point(278, 362);
			this.btnAddInputDetection.Name = "btnAddInputDetection";
			this.btnAddInputDetection.Size = new System.Drawing.Size(124, 28);
			this.btnAddInputDetection.TabIndex = 43;
			this.btnAddInputDetection.Text = "Add";
			this.btnAddInputDetection.UseVisualStyleBackColor = true;
			this.btnAddInputDetection.Click += new System.EventHandler(this.btnAddInputDetection_Click);
			// 
			// txtDetectionName
			// 
			this.txtDetectionName.Location = new System.Drawing.Point(227, 33);
			this.txtDetectionName.Name = "txtDetectionName";
			this.txtDetectionName.Size = new System.Drawing.Size(309, 26);
			this.txtDetectionName.TabIndex = 41;
			// 
			// cmbDetections
			// 
			this.cmbDetections.FormattingEnabled = true;
			this.cmbDetections.Location = new System.Drawing.Point(17, 362);
			this.cmbDetections.Name = "cmbDetections";
			this.cmbDetections.Size = new System.Drawing.Size(258, 28);
			this.cmbDetections.TabIndex = 42;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.Location = new System.Drawing.Point(13, 36);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(173, 20);
			this.label4.TabIndex = 43;
			this.label4.Text = "Detection object name:";
			// 
			// lsbInputsDetection
			// 
			this.lsbInputsDetection.FormattingEnabled = true;
			this.lsbInputsDetection.ItemHeight = 20;
			this.lsbInputsDetection.Location = new System.Drawing.Point(17, 396);
			this.lsbInputsDetection.Name = "lsbInputsDetection";
			this.lsbInputsDetection.Size = new System.Drawing.Size(519, 144);
			this.lsbInputsDetection.TabIndex = 41;
			// 
			// cmbGenericObjectSensors
			// 
			this.cmbGenericObjectSensors.FormattingEnabled = true;
			this.cmbGenericObjectSensors.Location = new System.Drawing.Point(227, 65);
			this.cmbGenericObjectSensors.Name = "cmbGenericObjectSensors";
			this.cmbGenericObjectSensors.Size = new System.Drawing.Size(309, 28);
			this.cmbGenericObjectSensors.TabIndex = 42;
			// 
			// frmMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1789, 981);
			this.Controls.Add(this.grbDetections);
			this.Controls.Add(this.grbAlarmWarning);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.grbConfig);
			this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.Name = "frmMain";
			this.Text = "Objecten genereren";
			this.Load += new System.EventHandler(this.cmbObjectType_Load);
			this.grbConfig.ResumeLayout(false);
			this.grbConfig.PerformLayout();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.grbAlarmWarning.ResumeLayout(false);
			this.grbAlarmWarning.PerformLayout();
			this.grbDetections.ResumeLayout(false);
			this.grbDetections.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnAddObject;
        private System.Windows.Forms.GroupBox grbConfig;
        private System.Windows.Forms.CheckBox chbObject;
        private System.Windows.Forms.CheckBox chbAlarmWarnings;
        private System.Windows.Forms.CheckBox chbDetections;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chbAlarmWarningsHMI;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.ComboBox cmbGenericObjects;
		private System.Windows.Forms.TextBox txtSoftwareObjectDescription;
		private System.Windows.Forms.TextBox txtSoftwareObjectGroup;
		private System.Windows.Forms.TextBox txtSoftwareObjectName;
		private System.Windows.Forms.ListBox lsbObjects;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Button btnRemove;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.ListBox lsbInputs;
		private System.Windows.Forms.ComboBox cmbOutputs;
		private System.Windows.Forms.ListBox lsbOutputs;
		private System.Windows.Forms.ComboBox cmbInputs;
		private System.Windows.Forms.Button btnRemoveOutput;
		private System.Windows.Forms.Button btnAddOutput;
		private System.Windows.Forms.Button btnRemoveInput;
		private System.Windows.Forms.Button btnAddInput;
		private System.Windows.Forms.Button btnAdd;
		private System.Windows.Forms.Button btnGetIO;
		private System.Windows.Forms.GroupBox grbAlarmWarning;
		private System.Windows.Forms.Button btnRemoveSoftwareUnit;
		private System.Windows.Forms.Button btnAddSoftwareUnit;
		private System.Windows.Forms.ListBox lsbSoftwareUnits;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.TextBox txtSoftwareUnit;
		private System.Windows.Forms.TextBox txtNumberOfWarnings;
		private System.Windows.Forms.TextBox txtNumberOfAlarms;
		private System.Windows.Forms.ListBox lsbAlarmWarningConfig;
		private System.Windows.Forms.TextBox txtText;
		private System.Windows.Forms.TextBox txtTag;
		private System.Windows.Forms.GroupBox grbDetections;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox txtDetectionName;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.ComboBox cmbGenericObjectSensors;
		private System.Windows.Forms.ListBox lsbDetections;
		private System.Windows.Forms.Button btnRemoveOutputDetection;
		private System.Windows.Forms.Button btnAddInputDetection;
		private System.Windows.Forms.ComboBox cmbDetections;
		private System.Windows.Forms.ListBox lsbInputsDetection;
		private System.Windows.Forms.Button btnRemoveDetection;
		private System.Windows.Forms.Button btnAddDetection;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.Button btnRestore;
	}
}

