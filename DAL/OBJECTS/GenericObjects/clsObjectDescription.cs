﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class clsObjectDescription
    {
        private string _sObjectName;
        private string _sObjectType;
        private string _sObjectDescription;
        private string _sGroup;


		public clsObjectDescription()
		{
		}

		public clsObjectDescription(string Name, string Type,string ObjectDescription, string Group)
        {
            _sObjectName = Name;
            _sObjectType = Type;
            _sObjectDescription = ObjectDescription ?? string.Empty;
            _sGroup = Group ?? string.Empty;
        }

		public override string ToString()
		{
			return _sObjectName + " - " + _sObjectType;
		}



		public string ObjectName { get { return _sObjectName; } set { _sObjectName = value; } } // Name of object
        public string ObjectType { get { return _sObjectType; } set { _sObjectType = value; } } // Name of PLC object Type
        public string ObjectDescription { get { return _sObjectDescription; } set { _sObjectDescription = value; } } // Description from object in excel
        public string Group { get { return _sGroup; } set { _sGroup = value; } } // Logical group of object - software unit
    }
}
