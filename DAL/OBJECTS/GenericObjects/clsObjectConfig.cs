﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class clsObjectConfig
    {
        private string _sConfigItem;

		public clsObjectConfig()
		{
		}

		public clsObjectConfig(string ConfigItem)
        {
            _sConfigItem = ConfigItem;
        }

        public string ConfigItem { get { return _sConfigItem; } set { _sConfigItem = value; } }

    }
}
