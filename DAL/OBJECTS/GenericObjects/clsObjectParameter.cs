﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class clsObjectParameter
    {

        string _sParName;
        string _sParType;
        string _sParComment;

		public clsObjectParameter()
		{
		}

		public clsObjectParameter(string ParName, string ParType, string ParComment)
        {
            _sParName = ParName;
            _sParType = ParType;
            _sParComment = ParComment;

        }

        public string ParameterName { get { return _sParName; } set { _sParName = value; } }
        public string ParameterType { get { return _sParType; } set { _sParType = value; } }
		public string ParameterComment { get { return _sParComment; } set { _sParComment = value; } }

	}
}
