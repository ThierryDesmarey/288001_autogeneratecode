﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class clsConfigFromFile
    {

        List<clsObjectDescription> _lstObjectDescription;
        List<string> _lstDI;
		List<clsInput> _lstDIInputs;
        List<string> _lstDO;
        List<clsInput> _lstAIInputs;
		List<string> _lstAI;
		List<string> _lstAO;
        List<clsSoftwareUnit> _lstSoftwareUnits;

		List<clsSoftwareObjectActuator> _lstSoftwareObjectActuators;
		List<clsSoftwareObjectDetection> _lstSoftwareObjectDetections;

		public clsConfigFromFile()
		{
		}

		public clsConfigFromFile(List<clsObjectDescription> ObjectDescription,List<clsInput> DI,List<string> DO,List<clsInput> AI,List<string> AO,
                                List<clsSoftwareUnit> SoftwareUnits)
        {
            _lstObjectDescription = ObjectDescription;
            _lstDIInputs=DI;
            _lstDO = DO;
            _lstAIInputs = AI;
            _lstAO = AO;
            _lstSoftwareUnits = SoftwareUnits;
        }

		public clsConfigFromFile( List<string> DI, List<string> DO, List<string> AI, List<string> AO)
		{
			_lstDI = DI;
			_lstDO = DO;
			_lstAI = AI;
			_lstAO = AO;
			_lstObjectDescription = new List<clsObjectDescription>();
			_lstSoftwareObjectActuators = new List<clsSoftwareObjectActuator>();
			_lstSoftwareObjectDetections = new List<clsSoftwareObjectDetection>();
			_lstSoftwareUnits = new List<clsSoftwareUnit>();

		}

		public void AddObjectDescription(List<clsObjectDescription> ObjectDescription)
		{
			_lstObjectDescription = ObjectDescription;
		}

		public void AddSoftwareObjectsActuators(List<clsSoftwareObjectActuator> SoftwareObjectActuators)
		{
			_lstSoftwareObjectActuators = SoftwareObjectActuators;
		}

		public void AddSoftwareUnit(List<clsSoftwareUnit> SoftwareUnits)
		{
			_lstSoftwareUnits = SoftwareUnits;
		}

		public void AddSoftwareObjectsDetections(List<clsSoftwareObjectDetection> SoftwareObjectDetections)
		{
			_lstSoftwareObjectDetections = SoftwareObjectDetections;
		}


		public List<clsObjectDescription> ObjectDiscription { get { return _lstObjectDescription; } set { _lstObjectDescription = value; } }
		public List<clsSoftwareObjectActuator> SoftwareObjectActuators { get { return _lstSoftwareObjectActuators; } set { _lstSoftwareObjectActuators = value; } }
		public List<clsSoftwareObjectDetection> SoftwareObjectDetections { get { return _lstSoftwareObjectDetections; } set { _lstSoftwareObjectDetections = value; } }

		public List<clsInput> DI { get { return _lstDIInputs	; } set { _lstDIInputs = value; } }
		public List<string> DI_Simple { get { return _lstDI; } set { _lstDI = value; } }
		public List<string> DO { get { return _lstDO; } set { _lstDO = value; } }
		public List<clsInput> AI { get { return _lstAIInputs; } set { _lstAIInputs = value; } }
		public List<string> AI_Simple { get { return _lstAI; } set { _lstAI = value; } }
		public List<string> AO { get { return _lstAO; } set { _lstAO = value; } }
		public List<clsSoftwareUnit> SoftwareUnits { get { return _lstSoftwareUnits; } set { _lstSoftwareUnits = value; } }




    }
}
