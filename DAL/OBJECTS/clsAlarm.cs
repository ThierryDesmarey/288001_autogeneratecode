﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class clsAlarm
    {
        private string _sAlarmText;
        private string _sAlarmTag;
        private int _iAlarmNumber;
        private string _sObject;
        private string _sCompleteAlarmTag;

		public clsAlarm()
		{
		}

		public clsAlarm(string AlarmText,string AlarmTag)
        {
            _sAlarmTag = AlarmTag;
            _sAlarmText = AlarmText;
        }

        public void CreateAlarmTag()
        {
            _sCompleteAlarmTag = "\"" +  string.Format("{0:0000}", _iAlarmNumber) + "_" + _sObject + "_" + _sAlarmTag + "\"";
        }

		public override string ToString()
		{
			return _sAlarmTag + " - " + _sAlarmText;
		}


		public string AlarmText { get { return _sAlarmText; } set { _sAlarmText = value; } }
        public string AlarmTag { get { return _sAlarmTag; } set { _sAlarmTag = value; } }
        public int AlarmNumber { set { _iAlarmNumber= value; } get { return _iAlarmNumber; } } // Alarmnumber in global Db
        public string LinkedObject { set { _sObject = value; } get { return _sObject; } } //Name of coupled object
        public string CompleteAlarmTag { get { return _sCompleteAlarmTag; } set { _sCompleteAlarmTag = value; } }
    }
}
