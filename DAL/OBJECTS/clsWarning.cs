﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class clsWarning
    {
        private string _sWarningText;
        private string _sWarningTag;
        private int _iWarningNumber;
        private string _sObject;
        private string _sCompleteWarningTag;

		public clsWarning()
		{
		}

		public clsWarning(string WarningText, string WarningTag)
        {
            _sWarningTag = WarningTag;
            _sWarningText = WarningText;
        }

        public void CreateWarningTag()
        {
            _sCompleteWarningTag = "\"" + string.Format("{0:0000}", _iWarningNumber) + "_" + _sObject + "_" + _sWarningTag + "\"" ;
        }

		public override string ToString()
		{
			return _sWarningTag + " - " + _sWarningText;
		}


		public  string WarningText { get { return _sWarningText; } set { _sWarningText = value; } }
        public string WarningTag { get { return _sWarningTag; } set { _sWarningTag = value; } }
        public int WarningNumber { set { _iWarningNumber = value; } get { return _iWarningNumber; } } //Warning Number in global DB
        public string LinkedObject { set { _sObject = value; } get { return _sObject; } } //Name of coupled object
        public string CompleteWarningTag { get { return _sCompleteWarningTag; } set { _sCompleteWarningTag = value; } } //
    }
}
