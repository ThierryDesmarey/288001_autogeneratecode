﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class clsIO
    {

        string _sName;

		public clsIO()
		{
		}

		public clsIO(string Name)
        {
            _sName = Name;
        }

        public string Name { get { return _sName; } set { _sName = value; } }


    }



    public class clsInput
    {

        string _sName;
        bool _xFB;


		public clsInput()
		{
		}

		public clsInput(string Name,bool FunctionBlock)
        {
            _sName = Name;
            _xFB = FunctionBlock;
        }

        public clsInput(string Name, object FunctionBlock)
        {
            _sName = Name;

            if (FunctionBlock != null)
            {
                if (Convert.ToString(FunctionBlock) == "x") { _xFB = true; } else { _xFB = false; }
            }
            else
            {
                _xFB = false;
            }
        }

        public string Name { get { return _sName; } set { _sName = value; } }
        public bool FunctionBlock { get { return _xFB; } set { _xFB = value; } }
        public string InputPar { get { return _sName; } set { _sName = value; } }

    }



}
