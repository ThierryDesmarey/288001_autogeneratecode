﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{ 
	public class clsObjectParameterConfig
	{
		private string _sParName;
		private string _sIOPoint;

		public clsObjectParameterConfig()
		{
		}

		public clsObjectParameterConfig(string ParName,string IOPoint)
		{
			_sParName=ParName;
			_sIOPoint = IOPoint ?? string.Empty;			
		}

		public override string ToString()
		{
			return _sParName + " := " + _sIOPoint;
		}

		public string ParName { get { return _sParName; } set { _sParName = value; } }
		public string IOPoint { get { return _sIOPoint; } set { _sIOPoint = value; } }

	}
}
