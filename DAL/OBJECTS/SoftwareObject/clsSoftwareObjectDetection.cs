﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
	public class clsSoftwareObjectDetection
	{

		private string _sSoftwareObjectName;				// Name of the software object
		private clsGenericObjectType _oGenericObjectType;	// Generic object type
		private List<clsObjectParameterConfig> _lstInputs;
		private List<clsObjectParameterConfig> _lstOutputs;

		public clsSoftwareObjectDetection()
		{
		}

		public clsSoftwareObjectDetection(clsGenericObjectType GenericObjectType, string SoftwareObjectName)
		{
			_sSoftwareObjectName = SoftwareObjectName;
			_oGenericObjectType = GenericObjectType;
			_lstInputs = new List<clsObjectParameterConfig>();
			_lstOutputs = new List<clsObjectParameterConfig>();

			foreach (clsObjectParameter input in _oGenericObjectType.Inputs)
			{
				_lstInputs.Add(new clsObjectParameterConfig(input.ParameterName, ""));
			}

			foreach (clsObjectParameter output in _oGenericObjectType.Outputs)
			{
				_lstOutputs.Add(new clsObjectParameterConfig(output.ParameterName, ""));
			}

		}

		public clsSoftwareObjectDetection(clsGenericObjectType GenericObjectType,string SoftwareObjectName, List<clsObjectParameterConfig> InputConfig, List<clsObjectParameterConfig> OutputConfig)
		{
			_sSoftwareObjectName = SoftwareObjectName;
			_oGenericObjectType = GenericObjectType;
			_lstInputs = InputConfig;
			_lstOutputs = OutputConfig;
		}

		public override string ToString()
		{
			return _sSoftwareObjectName;
		}


		public string SoftwareObjectName { get { return _sSoftwareObjectName; } set { _sSoftwareObjectName = value; } }
		public clsGenericObjectType GenericObjectType { get { return _oGenericObjectType; } set { _oGenericObjectType = value; } }
		public List<clsObjectParameterConfig> InputConfig { get { return _lstInputs; } set { _lstInputs = value; } }
		public List<clsObjectParameterConfig> OutputConfig { get { return _lstOutputs; } set { _lstOutputs = value; } }

	}
}
