﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class clsSoftwareUnit
    {
        private string _sName;
        private int _iNumberOfAlarms;
		private int _iNumberOfWarnings;
		private List<string> _lstAlarmText;
        private List<string> _lstAlarmTag;
        private List<string> _lstWarningText;
        private List<string> _lstWarningTag;

		private List<clsAlarm> _lstAlarmCfg;
		private List<clsWarning> _lstWarningCfg;


		public clsSoftwareUnit()
		{
		}

		public clsSoftwareUnit(string Name, int NumberOfAlarms, int NumberOfWarnings)
		{
			_sName = Name;
			_iNumberOfAlarms = NumberOfAlarms;
			_iNumberOfWarnings = NumberOfWarnings;

			_lstAlarmCfg = new List<clsAlarm>();
			_lstWarningCfg = new List<clsWarning>();

			for (int i = 0; i < NumberOfAlarms; i++)
			{
				_lstAlarmCfg.Add(new clsAlarm(string.Empty,"xAlr"));
			}

			for (int i = 0; i < NumberOfWarnings; i++)
			{
				_lstWarningCfg.Add(new clsWarning( string.Empty, "xWrn"));
			}
		}


		public clsSoftwareUnit(string Name, int NumberOfAlarms, List<string> lstAlarmText, List<string> lstAlarmTag, 
                                List<string> lstWarningText, List<string> lstWarningTag)
        {
            _sName = Name;
			_iNumberOfAlarms = NumberOfAlarms;
			//_iNumberOfWarnings = NumberOfWarnings;

			_lstAlarmText = lstAlarmText;
            _lstAlarmTag = lstAlarmTag;
            _lstWarningText = lstWarningText;
            _lstWarningTag = lstWarningTag;
		}

		public override string ToString()
		{
			return _sName;
		}

		public string Name { get { return _sName; } set { _sName = value; } }

        public int NumberOfAlarms { get { return _iNumberOfAlarms; } set { _iNumberOfAlarms = value; } }
		public int NumberOfWarnings { get { return _iNumberOfWarnings; } set { _iNumberOfWarnings = value; } }

		public List<clsAlarm> AlarmCfg { get { return _lstAlarmCfg; } set { _lstAlarmCfg = value; } }
		public List<clsWarning> WarningCfg { get { return _lstWarningCfg; } set { _lstWarningCfg = value; } }


		public List<string> AlarmText { get { return _lstAlarmText; } set { _lstAlarmText = value; } }
        public List<string> AlarmTags { get { return _lstAlarmTag; } set { _lstAlarmTag = value; } }
        public List<string> WarningText { get { return _lstWarningText; } set { _lstWarningText = value; } }
        public List<string> WarningTags { get { return _lstWarningTag; } set { _lstWarningTag = value; } }

    }
}
