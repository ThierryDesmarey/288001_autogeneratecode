﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class clsAnalogSensor
    {

        private string _sName;

		public clsAnalogSensor()
		{

		}

		public clsAnalogSensor(string Name)
        {
            _sName = Name;
        }

        public string Name { get { return _sName; } set { _sName = value; } }


    }
}
