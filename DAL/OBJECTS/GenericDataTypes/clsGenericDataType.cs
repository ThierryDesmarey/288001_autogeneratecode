﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class clsGenericDataType
    {
        private string _sObjectName;
        private string _sName;
        private List<clsObjectParameter> _lstInputs;
        private List<clsObjectParameter> _lstOutputs;
        private List<clsObjectParameter> _lstInOuts;
        private List<clsAlarm> _lstAlarms;
        private List<clsWarning> _lstWarnings;
        private List<clsObjectConfig> _lstObjectConfig;

		public clsGenericDataType()
		{
		}
		public clsGenericDataType(string ObjectName)
        {
            _sObjectName = ObjectName;
            _lstInputs = Inputs;
            _lstOutputs = Outputs;
            _lstInOuts = Inouts;
            _lstAlarms = Alarms;
            _lstWarnings = Warnings;
            _lstObjectConfig = ObjectConfig;            
        }

        public clsGenericDataType(string ObjectName, List<string> Inputs, List<string> Outputs, List<string> Inouts,
                        List<clsAlarm> Alarms, List<clsWarning> Warnings, List<clsObjectConfig> ObjectConfig)
        {
            // Voorlopig om verder te kunnen
        }

        public clsGenericDataType(string ObjectName, List<clsObjectParameter> Inputs, List<clsObjectParameter> Outputs, List<clsObjectParameter> Inouts)
        {
            _sObjectName = ObjectName;
            _lstInputs = Inputs;
            _lstOutputs = Outputs;
            _lstInOuts = Inouts;

            // Klasse kan vereenvoudigd worden 
            _lstAlarms = new List<clsAlarm>();
            _lstWarnings = new List<clsWarning>();

            CreateAlarmAndWarningList();
            
            // Reserved for future use
            _lstObjectConfig = new List<clsObjectConfig>();
        }


        private void CreateAlarmAndWarningList()
        {
            // Check outputs for error bits and warning bits

            foreach (clsObjectParameter output in _lstOutputs)
            {
                if (output.ParameterName.Contains("q_xErr"))
                {
                    _lstAlarms.Add(new clsAlarm(output.ParameterComment,output.ParameterName.Remove(0,2)));
                }
                else if (output.ParameterName.Contains("q_xWarn"))
                {
                    _lstWarnings.Add(new clsWarning(output.ParameterComment, output.ParameterName.Remove(0, 2)));
                }
            }
        }
		
        public override string ToString()
        {
            if (_sName != null) { return _sName + " - " + _sObjectName; }else{ return _sObjectName; }
        }

        public string Name { set { _sName = value; } get { return _sName; }}
        public string ObjectName { get { return _sObjectName; } set { _sObjectName = value; } }
        public List<clsObjectParameter> Inputs { get { return _lstInputs; } set { _lstInputs = value; } }
        public List<clsObjectParameter> Outputs { get { return _lstOutputs; } set { _lstOutputs = value; } }
        public List<clsObjectParameter> Inouts { get { return _lstInOuts; } set { _lstInOuts = value; } }
        public List<clsAlarm> Alarms { get { return _lstAlarms; } set { _lstAlarms = value; } }
        public List<clsWarning> Warnings { get { return _lstWarnings; } set { _lstWarnings = value; } }
        public List<clsObjectConfig> ObjectConfig { get { return _lstObjectConfig; } set { _lstObjectConfig = value; } }
    }
}
