﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using System.IO;

namespace DAL
{
    public static class clsExcel_DA
    {

        private static Application _oExcelApplication;
        private static Workbook _oWbk;
        private static Worksheet _oWshObjecten;
        private static Worksheet _oWshIO;
        private static Worksheet _oWshAlarmen;
        private static Worksheet _oWshWarning;


        #region "Objects"

        public static clsConfigFromFile ReadProjectFile(string Path)
        {            
            _oWbk = _oExcelApplication.Workbooks.Open(Path);
            _oWshObjecten = _oWbk.Worksheets[1];
            _oWshIO = _oWbk.Worksheets[2];
            _oWshAlarmen = _oWbk.Worksheets[3];
            _oWshWarning = _oWbk.Worksheets[4];

            List<clsObjectDescription> lstObjectDescription;
            lstObjectDescription = new List<clsObjectDescription>();
            List<clsInput> lstDI;
            lstDI = new List<clsInput>();
            List<string> lstDO;
            lstDO = new List<string>();
            List<clsInput> lstAI;
            lstAI = new List<clsInput>();
            List<string> lstAO;
            lstAO = new List<string>();
            List<clsSoftwareUnit> lstSoftwareUnits;
            lstSoftwareUnits = new List<clsSoftwareUnit>();

            int iIndex;
            int iIndexAlarmen;
            int iIndexWarnings;

            // Read objects from file
            iIndex = 2;
            iIndexAlarmen = 2;
            iIndexWarnings = 2;

            do
            {
                // Object description
                if (_oWshObjecten.Cells[iIndex, 1].Value != null)
                {
                    lstObjectDescription.Add(new clsObjectDescription(_oWshObjecten.Cells[iIndex, 1].Value.ToString(), _oWshObjecten.Cells[iIndex, 4].Value.ToString(), _oWshObjecten.Cells[iIndex, 3].Value.ToString(), _oWshObjecten.Cells[iIndex, 2].Value.ToString()));
                }
                else // Group description for alarms and warnings
                {

                    // Add alarm tags
                    List<string> lstAlarmSoftwareUnit;
                    lstAlarmSoftwareUnit = new List<string>();
                    List<string> lstAlarmTagSoftwareUnit;
                    lstAlarmTagSoftwareUnit = new List<string>();
                    // Add warning tags
                    List<string> lstWarningSoftwareUnit;
                    lstWarningSoftwareUnit = new List<string>();
                    List<string> lstWarningTagSoftwareUnit;
                    lstWarningTagSoftwareUnit = new List<string>();

                    // Check alarms of software units
                    do
                    { 
                        lstAlarmSoftwareUnit.Add(_oWshAlarmen.Cells[iIndexAlarmen, 2].Value.ToString());
                        lstAlarmTagSoftwareUnit.Add(_oWshAlarmen.Cells[iIndexAlarmen, 3].Value.ToString());

                        iIndexAlarmen += 1;
                    } while (_oWshAlarmen.Cells[iIndexAlarmen, 1].Value == _oWshObjecten.Cells[iIndex, 2].Value.ToString());

                    // Check warnings of software units
                    do
                    {
                        lstWarningSoftwareUnit.Add(_oWshWarning.Cells[iIndexWarnings, 2].Value.ToString());
                        lstWarningTagSoftwareUnit.Add(_oWshWarning.Cells[iIndexWarnings, 3].Value.ToString());

                        iIndexWarnings += 1;
                    } while (_oWshWarning.Cells[iIndexWarnings, 1].Value == _oWshObjecten.Cells[iIndex, 2].Value.ToString());

                    // Add software unit class
                    lstSoftwareUnits.Add(
                        new clsSoftwareUnit(_oWshObjecten.Cells[iIndex, 2].Value.ToString(), 
                        Convert.ToInt32(_oWshObjecten.Cells[iIndex, 5].Value.ToString()), 
                        lstAlarmSoftwareUnit,lstAlarmTagSoftwareUnit,
                        lstWarningSoftwareUnit,lstWarningTagSoftwareUnit));
                }      

                iIndex += 1;
            } while (_oWshObjecten.Cells[iIndex, 2].Value != null);

            // Read DI from file
            iIndex = 2;
            do
            {
                if (_oWshIO.Cells[iIndex, 1].Value != null)
                {
                    lstDI.Add(new clsInput(_oWshIO.Cells[iIndex, 1].Value.ToString(), _oWshIO.Cells[iIndex, 2].Value));
                }

                iIndex += 1;
            } while (_oWshIO.Cells[iIndex, 1].Value != null);


            // Read D0 from file
            iIndex = 2;
            do
            {
                if (_oWshIO.Cells[iIndex, 3].Value != null)
                {
                    lstDO.Add(_oWshIO.Cells[iIndex, 3].Value.ToString());
                }

                iIndex += 1;
            } while (_oWshIO.Cells[iIndex, 3].Value != null);


            // Read AI from file
            iIndex = 2;
            do
            {
                if (_oWshIO.Cells[iIndex, 5].Value != null)
                {
                    lstAI.Add(_oWshIO.Cells[iIndex, 5].Value.ToString());
                }

                iIndex += 1;
            } while (_oWshIO.Cells[iIndex, 5].Value != null);


            // Read AO from file
            iIndex = 2;
            do
            {
                if (_oWshIO.Cells[iIndex, 7].Value != null)
                {
                    lstAO.Add(_oWshIO.Cells[iIndex, 7].Value.ToString());
                }

                iIndex += 1;
            } while (_oWshIO.Cells[iIndex, 7].Value != null);

            clsConfigFromFile oConfigFromFile;
            oConfigFromFile = new clsConfigFromFile(lstObjectDescription,lstDI,lstDO,lstAI,lstAO,lstSoftwareUnits);

            return oConfigFromFile;
        }


#endregion

#region "WinCC"

        public static void WriteAlarmFile(string SourcePath,string FileName,List<clsAlarm> lstAlarm, List<clsWarning> lstWarning)
        {
            // Copy template and rename
            string sSource= SourcePath + @"\TemplateAlarms.xlsx";
            string sDestination = FileName.Replace(".scl", "") + "_Alarms.xlsx";
            File.Copy(sSource, sDestination);
                        
            // Open file
            _oWbk = _oExcelApplication.Workbooks.Open(sDestination);
            _oWshObjecten = _oWbk.Worksheets[1];

            int iWordNumber=0;
            int iTriggerBit=8;
            int iRow = 1;

            // Alarms
            for (int i = 0; i < lstAlarm.Count; i++)
            {
                iRow +=  1;
                // ID
                _oWshObjecten.Cells[iRow, 1].Value = lstAlarm[i].AlarmNumber;

                // Alarm text
                _oWshObjecten.Cells[iRow, 3].Value = lstAlarm[i].LinkedObject + ": " + lstAlarm[i].AlarmText;

                // Class
                _oWshObjecten.Cells[iRow, 5].Value = "Errors";

                // Trigger tag
                _oWshObjecten.Cells[iRow, 6].Value = "arrwAlarm";
                    
                // Trigger bit
                _oWshObjecten.Cells[iRow, 7].Value = iTriggerBit + iWordNumber * 16;

                // Group
                _oWshObjecten.Cells[iRow, 12].Value = "Alarm_group_1";

                // Trigger bit and word number
                if (iTriggerBit == 15)
                {
                    iTriggerBit = 0;
                }
                else if (iTriggerBit == 7)
                {
                    iTriggerBit = 8;
                    iWordNumber += 1;
                }
                else
                {
                    iTriggerBit += 1;
                }
            }

            iWordNumber = 0;
            iTriggerBit = 8;
            
            // Warnings
            for (int i = 0; i < lstWarning.Count; i++)
            {
                iRow += 1;
                // ID
                _oWshObjecten.Cells[iRow, 1].Value = lstWarning[i].WarningNumber;

                // Alarm text
                _oWshObjecten.Cells[iRow, 3].Value = lstWarning[i].LinkedObject + ": " + lstWarning[i].WarningText;

                // Class
                _oWshObjecten.Cells[iRow, 5].Value = "Warnings";

                // Trigger tag
                _oWshObjecten.Cells[iRow, 6].Value = "arrwWarning";

                // Trigger bit
                _oWshObjecten.Cells[iRow, 7].Value = iTriggerBit + iWordNumber * 16;

                // Trigger bit and word number
                if (iTriggerBit == 15)
                {
                    iTriggerBit = 0;
                }
                else if (iTriggerBit == 7)
                {
                    iTriggerBit = 8;
                    iWordNumber += 1;
                }
                else
                {
                    iTriggerBit += 1;
                }
            }

        }

#endregion

#region "General"

        public static void OpenExcel()
        {
            _oExcelApplication = new Application();           
        }

        public static void CloseExcel(bool SaveChanges)
        {

            if (_oExcelApplication != null)
            {
                _oWbk.Close(SaveChanges);
                _oWshObjecten = null;
                _oWshIO = null;               
                _oWbk = null;
                _oExcelApplication.Quit();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(_oExcelApplication);
                _oExcelApplication = null;
            }

        }

#endregion

    }
}
