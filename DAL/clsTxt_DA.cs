﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace DAL
{
    public static class clsTxt_DA
    {

        private static StreamWriter _oSW;
        private static StreamWriter _oSW_AWL;
        private static List<clsDet> _lstDetections;
        private static List<clsAnalogSensor> _lstAnalogSensors;

        public static void CreateFile(string Path)
        {
            _oSW = new StreamWriter(Path);
            _lstDetections = new List<clsDet>();
            _lstAnalogSensors = new List<clsAnalogSensor>();
        }

        #region "Alarm & Warning DB"

        public static void AddAlarmDB(clsConfigFromFile ConfigFromFile, List<clsGenericObjectType> Objecten,  List<clsAlarm> lstAlarm)
        {
            string sType;

            //Create DB
            _oSW.WriteLine("DATA_BLOCK DB_ERRORS");
            sType = "A";

            _oSW.WriteLine("{S7_Optimized_Access := 'FALSE'}");
            _oSW.WriteLine("");
            _oSW.WriteLine("STRUCT");

            // Create spare alarms/warnings
            for (int i = 0; i < lstAlarm.Count; i++)
            {
                _oSW.WriteLine(lstAlarm[i].CompleteAlarmTag + ":BOOL;");
            }

            _oSW.WriteLine("END_STRUCT;");
            _oSW.WriteLine("BEGIN");
            _oSW.WriteLine("END_DATA_BLOCK");
            _oSW.WriteLine("");
        }

        public static void AddWarningDB(clsConfigFromFile ConfigFromFile, List<clsGenericObjectType> Objecten,  List<clsWarning> lstWarning)
        {
            string sType;

            //Create DB
            _oSW.WriteLine("DATA_BLOCK DB_WARNINGS");
            sType = "W";

            _oSW.WriteLine("{S7_Optimized_Access := 'FALSE'}");
            _oSW.WriteLine("");
            _oSW.WriteLine("STRUCT");

            // Create spare alarms/warnings
            for (int i = 0; i < lstWarning.Count; i++)
            {
                _oSW.WriteLine(lstWarning[i].CompleteWarningTag + ":BOOL;");
            }

            _oSW.WriteLine("END_STRUCT;");
            _oSW.WriteLine("BEGIN");
            _oSW.WriteLine("END_DATA_BLOCK");
            _oSW.WriteLine("");
        }

        #endregion


        #region "Objects" 

        public static void AddObjectDB(clsConfigFromFile ConfigFromFile, List<clsGenericObjectType> Objecten)
        {
            _oSW.WriteLine("DATA_BLOCK DB_Objects");

            // 1200 - 1500
            _oSW.WriteLine("{S7_Optimized_Access := 'TRUE'}");
            _oSW.WriteLine("");
            _oSW.WriteLine("VAR RETAIN");

            //For each object in file create 
            for (int i = 0; i < ConfigFromFile.ObjectDiscription.Count; i++)
            {
				clsGenericObjectType ObjectType = Objecten.Find(x => x.ObjectName == ConfigFromFile.ObjectDiscription[i].ObjectType);
				_oSW.WriteLine(ConfigFromFile.ObjectDiscription[i].Group + "_" + ConfigFromFile.ObjectDiscription[i].ObjectName + ":" + ConfigFromFile.ObjectDiscription[i].ObjectType.Replace("FB_", "UDT_") + "; // " + ConfigFromFile.ObjectDiscription[i].ObjectDescription);           
            }

            _oSW.WriteLine("END_VAR");
            _oSW.WriteLine("BEGIN");
            _oSW.WriteLine("END_DATA_BLOCK");
            _oSW.WriteLine("");

		}

        public static void AddObjectFB(clsConfigFromFile ConfigFromFile, List<clsGenericObjectType> Objecten, 
                                        List<clsAlarm> lstAlarm, List<clsWarning> lstWarning)
        {
                _oSW.WriteLine("FUNCTION_BLOCK FB_OBJ");
                _oSW.WriteLine("VAR_TEMP");
                _oSW.WriteLine("END_VAR");
                _oSW.WriteLine("VAR");

                // Add FB instances of objects
                for (int i = 0; i < ConfigFromFile.ObjectDiscription.Count; i++)
                {
					if (!ConfigFromFile.ObjectDiscription[i].ObjectType.Contains("Sensor"))
					{
						// FB INSTANCE
						_oSW.WriteLine(ConfigFromFile.ObjectDiscription[i].Group + "_" + ConfigFromFile.ObjectDiscription[i].ObjectName + ":" + ConfigFromFile.ObjectDiscription[i].ObjectType + ";");
					}
                    
                }

                _oSW.WriteLine("END_VAR");

                // Add FB block calls
                for (int i = 0; i < ConfigFromFile.ObjectDiscription.Count; i++)
                {

						_oSW.WriteLine("// Block call " + ConfigFromFile.ObjectDiscription[i].Group + " - " + ConfigFromFile.ObjectDiscription[i].ObjectName);
						_oSW.WriteLine("//-----------");
						_oSW.WriteLine("#" + ConfigFromFile.ObjectDiscription[i].Group + "_" + ConfigFromFile.ObjectDiscription[i].ObjectName + "(");
						clsGenericObjectType ObjectType = Objecten.Find(x => x.ObjectName == ConfigFromFile.ObjectDiscription[i].ObjectType);

						// Try to add fysical inputs
						foreach (clsObjectParameterConfig input in ConfigFromFile.SoftwareObjectActuators[i].InputConfig)
						{
							if (input.IOPoint != string.Empty)
							{
								_oSW.WriteLine(input.ParName + ":=" + input.IOPoint + ",");
							}
						}

						// Try to add alarms
						AddAlarmToObject(ObjectType.Outputs, ConfigFromFile.ObjectDiscription[i].ObjectName, lstAlarm);

						// Try to add fysical outputs
						foreach (clsObjectParameterConfig output in ConfigFromFile.SoftwareObjectActuators[i].OutputConfig)
						{
							if (output.IOPoint != string.Empty)
							{
								_oSW.WriteLine(output.ParName + "=>" + output.IOPoint + ",");
							}
						}

						// Try to add warnings
						AddWarningToObject(ObjectType.Outputs, ConfigFromFile.ObjectDiscription[i].ObjectName, lstWarning);

						// Add object
						_oSW.WriteLine("iq_udtData:=DB_Objects." + ConfigFromFile.ObjectDiscription[i].Group + "_" + ConfigFromFile.ObjectDiscription[i].ObjectName + ");");

						// Free space at end of block call
						_oSW.WriteLine("");
					
                }

                _oSW.WriteLine("END_FUNCTION_BLOCK");
				_oSW.WriteLine("");

		}
				#endregion

		#region "Sensors"

		public static void AddDetectionsDB(clsConfigFromFile ConfigFromFile)
        {
            _oSW.WriteLine("DATA_BLOCK DB_Sensors");
            _oSW.WriteLine("{S7_Optimized_Access := 'TRUE'}");
            _oSW.WriteLine("");
            _oSW.WriteLine("STRUCT");

            //For each Detection in file create alarms
            for (int i = 0; i < ConfigFromFile.SoftwareObjectDetections.Count; i++)
            {
				clsObjectParameter oData = ConfigFromFile.SoftwareObjectDetections[i].GenericObjectType.Inouts.Find(o => o.ParameterName == "iq_udtData");
				_oSW.WriteLine(ConfigFromFile.SoftwareObjectDetections[i].SoftwareObjectName + ":" + oData.ParameterType + ";");
            }

            _oSW.WriteLine("END_STRUCT;");
            _oSW.WriteLine("BEGIN");
            _oSW.WriteLine("END_DATA_BLOCK");
            _oSW.WriteLine("");
        }

		public static void AddDetectionsFB(clsConfigFromFile ConfigFromFile, List<clsAlarm> lstAlarm)
		{
			_oSW.WriteLine("FUNCTION_BLOCK FB_Sensors");
			_oSW.WriteLine("VAR_TEMP");
			_oSW.WriteLine("END_VAR");
			_oSW.WriteLine("VAR");

			// Add FB instances of objects
			// ---------------------------
			for (int i = 0; i < ConfigFromFile.SoftwareObjectDetections.Count; i++)
			{
				_oSW.WriteLine(ConfigFromFile.SoftwareObjectDetections[i].SoftwareObjectName + ":" + ConfigFromFile.SoftwareObjectDetections[i].GenericObjectType.ObjectName + ";");
			}

			_oSW.WriteLine("END_VAR");

			// Add FB block calls
			// -----------------
			for (int i = 0; i < ConfigFromFile.SoftwareObjectDetections.Count; i++)
			{
				_oSW.WriteLine("// Block call " + ConfigFromFile.SoftwareObjectDetections[i].SoftwareObjectName);
				_oSW.WriteLine("//-----------");
				_oSW.WriteLine("#" + ConfigFromFile.SoftwareObjectDetections[i].SoftwareObjectName + "(");

				for (int j = 0; j < ConfigFromFile.SoftwareObjectDetections[i].InputConfig.Count ; j++)
				{
					if (ConfigFromFile.SoftwareObjectDetections[i].InputConfig[j].IOPoint != string.Empty)
					{
						_oSW.WriteLine(ConfigFromFile.SoftwareObjectDetections[i].InputConfig[j].ParName + ":=" + ConfigFromFile.SoftwareObjectDetections[i].InputConfig[j].IOPoint + ",");
					}					
				}

				// Try to add alarms
				AddAlarmToObject(ConfigFromFile.SoftwareObjectDetections[i].GenericObjectType.Outputs, ConfigFromFile.SoftwareObjectDetections[i].SoftwareObjectName, lstAlarm);

				// Add object
				_oSW.WriteLine("iq_udtData:=DB_Sensors." + ConfigFromFile.SoftwareObjectDetections[i].SoftwareObjectName + ");");

				// Free space at end of block call
				_oSW.WriteLine("");
			}
			
			_oSW.WriteLine("END_FUNCTION_BLOCK");
			
		}


		#endregion

		#region SourcFilesObjecten

		// Read source files and create classes from standard objects
		public static clsGenericObjectType ReadObjectFromSource(string path)
        {

            StreamReader oSR;
            oSR = new StreamReader(path);
            clsGenericObjectType oPLCObject;

            bool xVarInputActive = false;
            bool xVarOutputActive = false;
            bool xVarInoutActive = false;

            char oSplitChar = '\x0022';
            string sObjectName = oSR.ReadLine().Split(oSplitChar)[1];
            List<clsObjectParameter> lstInputs = new List<clsObjectParameter>();
            List<clsObjectParameter> lstOutputs = new List<clsObjectParameter>();
            List<clsObjectParameter> lstInOuts = new List<clsObjectParameter>();

            while (oSR.EndOfStream == false)
            {
                string sLine = "";
                sLine = oSR.ReadLine();

                if (sLine.Contains("VAR_INPUT"))
                {
                    xVarInputActive = true;
                    xVarOutputActive = false;
                    xVarInoutActive = false;
                }
                else if (sLine.Contains("VAR_OUTPUT"))
                {
                    xVarInputActive = false;
                    xVarOutputActive = true;
                    xVarInoutActive = false;
                }
                else if (sLine.Contains("VAR_IN_OUT"))
                {
                    xVarInputActive = false;
                    xVarOutputActive = false;
                    xVarInoutActive = true;
                }
                else if (sLine.Contains("END_VAR"))
                {
                    xVarInputActive = false;
                    xVarOutputActive = false;
                    xVarInoutActive = false;
                }
                else if (sLine.Contains("BEGIN"))
                {
                    break;
                }
                else if (xVarInputActive)
                {                    
                    lstInputs.Add(GetIODetails(sLine));
                }
                else if (xVarOutputActive)
                {
                    lstOutputs.Add(GetIODetails(sLine));
                }
                else if (xVarInoutActive)
                {
                    lstInOuts.Add(GetIODetails(sLine));
                }

            }
            
            oPLCObject = new clsGenericObjectType(sObjectName, lstInputs, lstOutputs, lstInOuts);
            return oPLCObject;

        }

		public static clsGenericDataType ReadDataTypeFromSource(string path)
		{

			StreamReader oSR;
			oSR = new StreamReader(path);
			clsGenericDataType oDataType;

			List<string> lstStruct= new List<string>();

			char oSplitChar = '\x0022';
			string sObjectName = oSR.ReadLine().Split(oSplitChar)[1];

			List<clsDataTypeParameter> lstDataTypePar = new List<clsDataTypeParameter>();

			while (oSR.EndOfStream == false)
			{
				string sLine = "";
				sLine = oSR.ReadLine();

				if (sLine.Contains(": Struct"))
				{
					//lstDataTypePar.Add(GetDataTypeDetails(sLine));

				}
				else if (sLine.Contains("END_STRUCT"))
				{
					//lstStruct.Remove(lstStruct[lstStruct.Count() - 1]);
				}			


			}
			oDataType = new clsGenericDataType("");
			//oDataType = new clsGenericDataType(sObjectName, lstInputs, lstOutputs, lstInOuts);
			return oDataType;

		}

		#endregion


		#region IO

		public static clsConfigFromFile ReadConfig(string path)
		{
			StreamReader oSR;
			oSR = new StreamReader(path);

			string sObjectName = oSR.ReadLine();


			List<string> lstDI;
			lstDI = new List<string>();
			List<string> lstDO;
			lstDO = new List<string>();
			List<string> lstAI;
			lstAI = new List<string>();
			List<string> lstAO;
			lstAO = new List<string>();

			while (oSR.EndOfStream == false)
			{
				string sLine = "";
				sLine = oSR.ReadLine();

				char oSplitChar = ';';

				string[] sParts = sLine.Split(oSplitChar);

				if (sParts[0] != string.Empty)
				{
					lstDI.Add(sParts[0]);
				}

				if (sParts[1] != string.Empty)
				{
					lstDO.Add(sParts[1]);
				}

				if (sParts[2] != string.Empty)
				{
					lstAI.Add(sParts[2]);
				}

				if (sParts[3] != string.Empty)
				{
					lstAO.Add(sParts[3]);
				}

			}

			clsConfigFromFile oConfigFromFile;
			oConfigFromFile = new clsConfigFromFile( lstDI, lstDO, lstAI, lstAO);

			return oConfigFromFile;

		}

		#endregion

		public static void CloseFile()
        {
            _oSW.Close();
        }

        #region "Private"

		// Get IO details from object
        private static clsObjectParameter GetIODetails(string LineFromSourceFile)
        {
            clsObjectParameter oObjectParameter;
            string sLine = LineFromSourceFile;
            string[] sParts;
            string Parname;
            string ParType;
            string ParComment;

            // Get Name of IO
            sParts = sLine.Split(':');
            Parname = sParts[0].Trim();

            // Get datatype of IO
            sParts = sParts[1].Split(';');
            ParType = sParts[0].Trim();
            //Replace " by space and trim (necessairy for udt or structs
            sParts = ParType.Split('\x0022');

            if (sParts.Count() > 2)
            {
                ParType = sParts[1];
            }

            // Get comment of IO
            sParts = sLine.Split('/');

            if (sParts.Count() > 2)
            {
                ParComment = sParts[2].Trim();
            }
            else
            {
                ParComment = "";
            }

            oObjectParameter = new clsObjectParameter(Parname, ParType, ParComment);
            return oObjectParameter;
        }

		//private static clsDataTypeParameter GetDataTypeDetails(string LineFromSourceFile)
		

        private static void AddAlarmToObject(List<clsObjectParameter> IOofObject, string ObjectName,List<clsAlarm> lstAlarms)
        {
                // Try to add alarms
                for (int j = 0; j < IOofObject.Count; j++)
                {
                    string sVar = IOofObject[j].ParameterName.Remove(0, 2);

                    if (lstAlarms != null)
                    {
                        clsAlarm Alarm = lstAlarms.Find(x => x.LinkedObject == ObjectName && x.AlarmTag == sVar);

                        if (Alarm != null)
                        {
                            _oSW.WriteLine(IOofObject[j].ParameterName + "=>" + "DB_ERRORS." + Alarm.CompleteAlarmTag + ",");
                        }
                    }

                }
            
         }

        private static void AddWarningToObject(List<clsObjectParameter> IOofObject, string ObjectName,List<clsWarning> lstWarnings)
        {

                // Try to add inputs
                for (int j = 0; j < IOofObject.Count; j++)
                {
                    string sVar = IOofObject[j].ParameterName.Remove(0, 2);

                    if (lstWarnings != null)
                    {
                        clsWarning Warning = lstWarnings.Find(x => x.LinkedObject == ObjectName && x.WarningTag == sVar);

                        if (Warning != null)
                        {
                            _oSW.WriteLine(IOofObject[j].ParameterName + "=>" + "DB_WARNINGS." + Warning.CompleteWarningTag + ",");
                        }
                    }
                }            
		}

    
        #endregion




    }
}
