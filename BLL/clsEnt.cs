﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace BLL
{
    public static class clsEnt
    {
        private static List<clsAlarm> _lstAlarms;
        private static List<clsWarning> _lstWarnings;

        // Convert String to bool
        public static bool ConvertToBool(string s)
        {
            bool x;
            switch (s)
            {
                case "True":
                    x = true;
                    break;
                case "False":
                       x = false;
                    break;
                default:
                    x = false;
                    break;
            }

            return x;
        }

        //Convert PLC_Type to integer
        public static int ConvertoToPLCType(string s)
        {
            int i=0;
            if (s == "1200/1500")
            {
                i = 1;
            }
            if (s == "300/400 TIA")
            {
                i = 2;
            }
            if (s == "300/400")
            {
                i = 3;
            }
            return i;
        }

        // Get object prefix from objectname
        public static int GetObjectPrefix(string ObjectName, int Type)
        {
            int i = 0;
            
            if (ObjectName.Contains("Motor")) // Motors
            {
                i = 1000;
            }
            else if (ObjectName.Contains("Valve")) // Valves
            {
                i = 2000;
            }

            if (Type == 2) // Warning
            {
                    i += 5000;
            }

            return i;
        }
        
        // Generate alarm and warning lists
        public static void CreateAlarmWarningData(clsConfigFromFile ConfigFromFile, List<clsGenericObjectType> Objecten, int Type, int iSpare)
        {
            int iNumber = 1;			
            if (Type == 1)
            {
                _lstAlarms = new List<clsAlarm>();
            }
            else if (Type == 2)
            {
                _lstWarnings = new List<clsWarning>();
            }            

            // Standard warnings error OB's 
            // ----------------------------
            if (Type ==2)
            {
                // Spare
                _lstWarnings.Add(new clsWarning("OB80 Executed", "xExecuted"));
                _lstWarnings[iNumber - 1].WarningNumber = iNumber + 5000;
                _lstWarnings[iNumber - 1].LinkedObject = "OB80";
                _lstWarnings[iNumber - 1].CreateWarningTag();
                iNumber += 1;

                // Spare
                _lstWarnings.Add(new clsWarning("OB81 Executed", "xExecuted"));
                _lstWarnings[iNumber - 1].WarningNumber = iNumber + 5000;
                _lstWarnings[iNumber - 1].LinkedObject = "OB81";
                _lstWarnings[iNumber - 1].CreateWarningTag();
                iNumber += 1;

                // Spare
                _lstWarnings.Add(new clsWarning("OB82 Executed", "xExecuted"));
                _lstWarnings[iNumber - 1].WarningNumber = iNumber + 5000;
                _lstWarnings[iNumber - 1].LinkedObject = "OB82";
                _lstWarnings[iNumber - 1].CreateWarningTag();
                iNumber += 1;

                // Spare
                _lstWarnings.Add(new clsWarning("OB85 Executed", "xExecuted"));
                _lstWarnings[iNumber - 1].WarningNumber = iNumber + 5000;
                _lstWarnings[iNumber - 1].LinkedObject = "0B85";
                _lstWarnings[iNumber - 1].CreateWarningTag();
                iNumber += 1;

                // Spare
                _lstWarnings.Add(new clsWarning("OB86 Executed", "xExecuted"));
                _lstWarnings[iNumber - 1].WarningNumber = iNumber + 5000;
                _lstWarnings[iNumber - 1].LinkedObject = "OB86";
                _lstWarnings[iNumber - 1].CreateWarningTag();
                iNumber += 1;

                // Spare
                _lstWarnings.Add(new clsWarning("OB87 Executed", "xExecuted"));
                _lstWarnings[iNumber - 1].WarningNumber = iNumber + 5000;
                _lstWarnings[iNumber - 1].LinkedObject = "OB87";
                _lstWarnings[iNumber - 1].CreateWarningTag();
                iNumber += 1;

                // Spare
                _lstWarnings.Add(new clsWarning("OB121 Executed", "xExecuted"));
                _lstWarnings[iNumber - 1].WarningNumber = iNumber + 5000;
                _lstWarnings[iNumber - 1].LinkedObject = "OB121";
                _lstWarnings[iNumber - 1].CreateWarningTag();
                iNumber += 1;
                // Spare
                _lstWarnings.Add(new clsWarning("OB122 Executed", "xExecuted"));
                _lstWarnings[iNumber - 1].WarningNumber = iNumber + 5000;
                _lstWarnings[iNumber - 1].LinkedObject = "OB122";
                _lstWarnings[iNumber - 1].CreateWarningTag();
                iNumber += 1;
            }

			// Generate alarms / warnings for software units
			// ---------------------------------------------
			for (int i = 0; i < ConfigFromFile.SoftwareUnits.Count; i++)
			{
				if (Type == 1)
				{

					for (int j = 0; j < ConfigFromFile.SoftwareUnits[i].NumberOfAlarms; j++)
					{
						_lstAlarms.Add(ConfigFromFile.SoftwareUnits[i].AlarmCfg[j]);
						_lstAlarms[iNumber - 1].AlarmNumber = iNumber;
						_lstAlarms[iNumber - 1].LinkedObject = ConfigFromFile.SoftwareUnits[i].Name;
						_lstAlarms[iNumber - 1].CreateAlarmTag();
						iNumber += 1;
					}
				}

				if (Type == 2)
				{
					for (int j = 0; j < ConfigFromFile.SoftwareUnits[i].NumberOfWarnings; j++)
					{
						_lstWarnings.Add(ConfigFromFile.SoftwareUnits[i].WarningCfg[j]);
						_lstWarnings[iNumber - 1].WarningNumber = iNumber + 5000;
						_lstWarnings[iNumber - 1].LinkedObject = ConfigFromFile.SoftwareUnits[i].Name;
						_lstWarnings[iNumber - 1].CreateWarningTag();
						iNumber += 1;
					}
				}
			}

            // Create alarms for detections
            // ----------------------------

            if (Type == 1)
            {
				for (int i = 0; i < ConfigFromFile.SoftwareObjectDetections.Count; i++)
				{
					for (int j = 0; j < ConfigFromFile.SoftwareObjectDetections[i].GenericObjectType.Alarms.Count; j++)
					{
						_lstAlarms.Add(ConfigFromFile.SoftwareObjectDetections[i].GenericObjectType.Alarms[j]);
						_lstAlarms[iNumber - 1].AlarmNumber = iNumber;
						_lstAlarms[iNumber - 1].LinkedObject = ConfigFromFile.SoftwareObjectDetections[i].SoftwareObjectName;
						_lstAlarms[iNumber - 1].CreateAlarmTag();
						iNumber += 1;
					}
				}

            }

			// Create warnings for detections
			// ------------------------------
			if (Type == 2)
			{
				for (int i = 0; i < ConfigFromFile.SoftwareObjectDetections.Count; i++)
				{
					for (int j = 0; j < ConfigFromFile.SoftwareObjectDetections[i].GenericObjectType.Warnings.Count; j++)
					{
						_lstWarnings.Add(ConfigFromFile.SoftwareObjectDetections[i].GenericObjectType.Warnings[j]);
						_lstWarnings[iNumber - 1].WarningNumber = iNumber;
						_lstWarnings[iNumber - 1].LinkedObject = ConfigFromFile.SoftwareObjectDetections[i].SoftwareObjectName;
						_lstWarnings[iNumber - 1].CreateWarningTag();
						iNumber += 1;
					}
				}
			}

            // Calculate number of alarms or warnings to be added for objects
            int iNumberToBeAdded = 0;

            for (int i = 0; i < ConfigFromFile.ObjectDiscription.Count; i++)
            {
                clsGenericObjectType ObjectType = Objecten.Find(x => x.ObjectName == ConfigFromFile.ObjectDiscription[i].ObjectType);
                if (ObjectType != null)
                {
               
                    if (Type == 1)
                    {
                        for (int j = 0; j < ObjectType.Alarms.Count; j++)
                        {
                            iNumberToBeAdded += 1;
                        }

                    }
                    if (Type == 2)
                    {
                        for (int j = 0; j < ObjectType.Warnings.Count; j++)
                        {
                            iNumberToBeAdded += 1;
                        }
                    }

                }
            }

            // Add spare to get DB filled on byte level
            int iEnd;
            double dEnd;
            int iEndIndex;
            dEnd = Math.Floor(Convert.ToDouble(iNumber + iNumberToBeAdded) / 32);
            iEnd = (Convert.ToInt32(dEnd) + 1) * 32;
            iEndIndex = iEnd - iNumberToBeAdded;
            
            // Create spare alarms/warnings
            for (int i = iNumber; i <= iEndIndex; i++)
            {
                if (Type == 1)
                {
                    _lstAlarms.Add(new clsAlarm("", string.Format("{0:0}{1:00}", "xSpare", iNumber)));
                    _lstAlarms[iNumber - 1].AlarmNumber = iNumber;
                    _lstAlarms[iNumber - 1].LinkedObject = "";
                    _lstAlarms[iNumber - 1].CreateAlarmTag();
                    iNumber += 1;
                }

                if (Type == 2)
                {
                    _lstWarnings.Add(new clsWarning("", string.Format("{0:0}{1:00}", "xSpare", iNumber + 5000)));
                    _lstWarnings[iNumber - 1].WarningNumber = iNumber + 5000;
                    _lstWarnings[iNumber - 1].LinkedObject = "";
                    _lstWarnings[iNumber - 1].CreateWarningTag();
                    iNumber += 1;
                }
            }


            
			int iMessageNumberMotors= 0;
			int iMessageNumberValves = 0;

			// For each object in file create alarms or warnings
			// -------------------------------------------------
			for (int i = 0; i < ConfigFromFile.ObjectDiscription.Count; i++)
            {
                clsGenericObjectType ObjectType = Objecten.Find(x => x.ObjectName == ConfigFromFile.ObjectDiscription[i].ObjectType);
                if (Type == 1)
                {

                    for (int j = 0; j < ObjectType.Alarms.Count; j++)
                    {                      
                        _lstAlarms.Add(new clsAlarm(ObjectType.Alarms[j].AlarmText, ObjectType.Alarms[j].AlarmTag));

						if (ObjectType.ObjectName.Contains("Valve"))
						{
							_lstAlarms[iNumber - 1].AlarmNumber = iMessageNumberValves + GetObjectPrefix(ObjectType.ObjectName, 1);
						}
						else
						{
							_lstAlarms[iNumber - 1].AlarmNumber = iMessageNumberMotors + GetObjectPrefix(ObjectType.ObjectName, 1);
						}

                        _lstAlarms[iNumber - 1].LinkedObject = ConfigFromFile.ObjectDiscription[i].ObjectName;
                        _lstAlarms[iNumber - 1].CreateAlarmTag();
						if (ObjectType.ObjectName.Contains("Valve")) { iMessageNumberValves += 1; } else { iMessageNumberMotors += 1; }                        
                        iNumber += 1;
                    }

                }
                if (Type == 2)
                {

                    for (int j = 0; j < ObjectType.Warnings.Count; j++)
                    {
                        _lstWarnings.Add(new clsWarning(ObjectType.Warnings[j].WarningText, ObjectType.Warnings[j].WarningTag));

						if (ObjectType.ObjectName.Contains("Valve"))
						{
							_lstWarnings[iNumber - 1].WarningNumber = iMessageNumberValves + GetObjectPrefix(ObjectType.ObjectName, 1);
						}
						else
						{
							_lstWarnings[iNumber - 1].WarningNumber = iMessageNumberMotors + GetObjectPrefix(ObjectType.ObjectName, 1);
						}

                        _lstWarnings[iNumber - 1].LinkedObject = ConfigFromFile.ObjectDiscription[i].ObjectName;
                        _lstWarnings[iNumber - 1].CreateWarningTag();
						if (ObjectType.ObjectName.Contains("Valve")){iMessageNumberValves += 1;	}else{iMessageNumberMotors += 1;}						
                        iNumber += 1;
                    }
                }
            }

        }


        public static List<clsAlarm> Alarms { get { return _lstAlarms; } }
        public static List<clsWarning> Warnings { get { return _lstWarnings; } }

    }
}
