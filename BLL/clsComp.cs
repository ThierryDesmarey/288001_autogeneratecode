﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace BLL
{
    public static class clsComp
    {

        private static List<clsGenericObjectType> _lstGenericObjectTypes;
		private static List<clsGenericDataType> _lstGenericDataTypes;

		//private static List<>
		private static clsConfigFromFile _oConfigFromFile;
        private static bool _xAlarmsGenerated;
        private static bool _xWarningsGenerated;

		// Get generic types from tia portal sources
        public static void GetPLCObjectTypes(string StartUpPath)
        {
            // Get list of available PLC objects
            string sPath = StartUpPath + @"\OBJECTEN";
            string[] Files = System.IO.Directory.GetFiles(sPath);

			_lstGenericObjectTypes = new List<clsGenericObjectType>();
			_lstGenericDataTypes = new List<clsGenericDataType>();

			foreach (string File in Files)
			{
				char oSplitChar = '\x005C';
				string[] sParts = File.Split(oSplitChar);

				if (sParts[sParts.Count() - 1].Contains(".udt"))
				{
					_lstGenericDataTypes.Add(clsTxt_DA.ReadDataTypeFromSource(File));
				}
				else
				{
					_lstGenericObjectTypes.Add(clsTxt_DA.ReadObjectFromSource(File));
				}
            }

        }

		// Get IO configuration
        public static void GetProjectInfo(string Path)
        {
			_oConfigFromFile = clsTxt_DA.ReadConfig(Path);
        }

		// Configuration
		public static void GetProjectInfo(clsConfigFromFile oConfig)
		{
			_oConfigFromFile = oConfig;
		}

		// Set object description
		public static void SetObjectDiscription(List<clsObjectDescription> lstObjectDescription)
		{
			_oConfigFromFile.AddObjectDescription(lstObjectDescription);
		}

		// Set software objects actuators
		public static void SetSoftwareObjectActuators(List<clsSoftwareObjectActuator> lstSoftwareObjectActuators)
		{
			_oConfigFromFile.AddSoftwareObjectsActuators(lstSoftwareObjectActuators);
		}

		// Set software units
		public static void SetSoftwareUnits(List<clsSoftwareUnit> SoftwareUnits)
		{
			_oConfigFromFile.AddSoftwareUnit(SoftwareUnits);
		}

		// Set software object detections
		public static void SetSoftwareObjectDetections(List<clsSoftwareObjectDetection> lstSoftwareObjectDetections)
		{
			_oConfigFromFile.AddSoftwareObjectsDetections(lstSoftwareObjectDetections);
		}




		public static void CreateSources(bool AlarmWarning,  bool Objects, bool Detections, string Path)
		{
			clsTxt_DA.CreateFile(Path);

			if (AlarmWarning)
			{
				clsEnt.CreateAlarmWarningData(_oConfigFromFile, _lstGenericObjectTypes, 1, 0);
				_xAlarmsGenerated = true;
				clsTxt_DA.AddAlarmDB(_oConfigFromFile, _lstGenericObjectTypes, clsEnt.Alarms);

				clsEnt.CreateAlarmWarningData(_oConfigFromFile, _lstGenericObjectTypes, 2, 0);
				_xWarningsGenerated = true;
				clsTxt_DA.AddWarningDB(_oConfigFromFile, _lstGenericObjectTypes, clsEnt.Warnings);
			}

			// Add object DB & FB to source
			if (Objects)
			{
				clsTxt_DA.AddObjectDB(_oConfigFromFile, _lstGenericObjectTypes);
				clsTxt_DA.AddObjectFB(_oConfigFromFile, _lstGenericObjectTypes, clsEnt.Alarms, clsEnt.Warnings);
			}

			if (Detections)
			{
				clsTxt_DA.AddDetectionsDB(_oConfigFromFile);
				clsTxt_DA.AddDetectionsFB(_oConfigFromFile, clsEnt.Alarms);
			}

			clsTxt_DA.CloseFile();

		}

		public static void CreateHMIObjects(bool Alarm,string TemplateFile , string Path)
		{
			if (Alarm)
			{
				// Create alarm and or warning lists
				clsEnt.CreateAlarmWarningData(_oConfigFromFile, _lstGenericObjectTypes, 1, 0);
				clsEnt.CreateAlarmWarningData(_oConfigFromFile, _lstGenericObjectTypes, 2, 0);
				clsExcel_DA.OpenExcel();

				// Create file
				clsExcel_DA.WriteAlarmFile(TemplateFile, Path, clsEnt.Alarms, clsEnt.Warnings);

				// Close excel
				clsExcel_DA.CloseExcel(true);
			}		
		}

        public static void InitCodeGeneration()
        {
            _xAlarmsGenerated = false;
            _xWarningsGenerated = false;
        }

        public static List<clsGenericObjectType> GenericObjects { get { return _lstGenericObjectTypes; } }
        public static clsConfigFromFile ConfigFromFile { get { return _oConfigFromFile; } }

    }
}
